package com.zoundindustries.bt.connectionservice.service

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.bluetooth.le.ScanSettings.SCAN_MODE_LOW_LATENCY
import android.content.Context
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice.Type
import com.zoundindustries.bt.connectionservice.model.errors.AdidasThrowable
import com.zoundindustries.bt.connectionservice.utils.Utils.Companion.getDevicesListFromJson
import com.zoundindustries.bt.connectionservice.utils.logd
import io.reactivex.subjects.BehaviorSubject

const val MAN_ID = 0x5A06

const val MAN_DATA_HEADER = 0xFF.toByte()

/**
 * Class wrapping BLE scanning. Handles starting and stopping scan and emitting newly found devices.
 *
 * @property context used to handle scanning in the client context.
 * @constructor creates BLEScanner instance for the specified client.
 */
class BLEScanner(val context: Context) {

    //region properties
    val newDevice: BehaviorSubject<AdidasBluetoothDevice> = BehaviorSubject.create()

    private val bluetoothAdapter: BluetoothAdapter =
            (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
    private var scanInProgress: Boolean = false

    private lateinit var leScanCallback: ScanCallback
    //endregion


    init {
        setupLEScanCallback()
    }

    //region public functions
    /**
     * Starts scanning for all available Adidas BLE headphones.
     */
    fun startScan(deviceId: String?) {

        if (!scanInProgress) {
            bluetoothAdapter.bluetoothLeScanner.startScan(
                    listOf(ScanFilter.Builder()
                            .setManufacturerData(MAN_ID, ByteArray(0))
                            .setDeviceAddress(deviceId)
                            .build()),
                    ScanSettings.Builder()
                            .setScanMode(SCAN_MODE_LOW_LATENCY)
                            .build(),
                    leScanCallback)
            scanInProgress = true
        }
    }

    /**
     * Get the full AdidasBluetoothDevice form the system based on [deviceId]. Type of the device is
     * retrieved from the storage to avoid scanning for already configured device.
     * @return the requested device matching the passed [deviceId]
     */
    fun getRemoteDeviceFromId(deviceId: String?): AdidasBluetoothDevice? {
        if (deviceId.isNullOrEmpty()) {
            return null
        }

        val remoteDevice: BluetoothDevice

        try {
            remoteDevice = bluetoothAdapter.getRemoteDevice(deviceId)
        } catch (e: IllegalStateException) {
            return null
        }

        val deviceInfo =
                getDevicesListFromJson(context).find { device -> device.address == deviceId }

        deviceInfo?.type?.let { type ->
            return AdidasBluetoothDevice(remoteDevice, type)
        }
        return null
    }

    /**
     * Get list of currently paired devices from the device.
     * @return paired devices.
     */
    fun getPairedDevices(): MutableSet<BluetoothDevice>? {
        return bluetoothAdapter.bondedDevices
    }

    /**
     * Return flag indicating Bluetooth enabled/disabled on the device.
     */
    fun isSystemBluetoothEnabled(): Boolean {
        return bluetoothAdapter.isEnabled
    }

    /**
     * Stops scanning for all available Adidas BLE headphones.
     */
    fun stopScan() {
        if (scanInProgress) {
            bluetoothAdapter.bluetoothLeScanner.stopScan(leScanCallback)
            scanInProgress = false
        }
    }
    //endregion

    private fun setupLEScanCallback() {
        leScanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult?) {
                super.onScanResult(callbackType, result)
                result ?: return

                newDevice.onNext(AdidasBluetoothDevice(
                        result.device,
                        getTypeFromAdvertisementData(result)))

                logd("onScanResult: <name>: ${result.device.name}")
            }

            override fun onScanFailed(errorCode: Int) {
                super.onScanFailed(errorCode)
                scanInProgress = false
                newDevice.onError(AdidasThrowable(AdidasThrowable.ErrorType.SCAN_FAILED, "scanFailed"))
                logd("onScanFailed: <errorCode>: $errorCode")
            }
        }
    }

    private fun getTypeFromAdvertisementData(result: ScanResult): AdidasBluetoothDevice.Type {
        result.scanRecord?.bytes?.let { bytes ->
            return AdidasBluetoothDevice.Type.fromInt(getDeviceTypeOffset(bytes))
        }
        throw java.lang.IllegalStateException("WRONG TYPE")
    }

    private fun getDeviceTypeOffset(bytes: ByteArray): Int {
        var offset = 0

        while (bytes[offset + 1] != MAN_DATA_HEADER) {
            offset += bytes[offset].toInt()
            offset = offset.inc()
        }

        return bytes[offset + 4].toInt()
    }
}