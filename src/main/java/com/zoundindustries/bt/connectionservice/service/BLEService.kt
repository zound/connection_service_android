package com.zoundindustries.bt.connectionservice.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.zoundindustries.bt.connectionservice.model.CommonPreferences
import com.zoundindustries.bt.connectionservice.model.SystemBluetoothStateReceiver
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice.ConnectionState
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.batterylevel.BatteryServiceWrapper
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.batterylevel.CHARACTERISTIC_BATTERY_LEVEL
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.*
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.deviceinformation.*
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.ota.CHARACTERISTIC_OTA_COMMAND
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.ota.CHARACTERISTIC_OTA_RESPONSE
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.ota.OTAServiceWrapper
import com.zoundindustries.bt.connectionservice.model.ota.OTAManager
import com.zoundindustries.bt.connectionservice.model.ota.OTAUpgradeState
import com.zoundindustries.bt.connectionservice.model.spotify.SpotifyAppRemoteController
import com.zoundindustries.bt.connectionservice.utils.logd
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

private const val SERVICE_DISPOSE_DELAY: Long = 2000

/**
 * Class handling communication with currently connected AdidasBluetoothDevice via Android BLE API.
 * It provides characteristics data via RX API.
 */
class BLEService : Service() {

    //region properties

    private lateinit var device: AdidasBluetoothDevice

    /**
     * Controller for spotify app remote library
     */
    var spotifyAppRemoteController = SpotifyAppRemoteController(this)

    /**
     * Wrapper on battery GATT service providing API to device current battery level.
     */
    var batteryServiceWrapper: BatteryServiceWrapper? = null
        private set

    /**
     * Wrapper on device information GATT service providing API to device name, FW, HW versions, etc.
     */
    var informationServiceWrapper: DeviceInformationServiceWrapper? = null
        private set

    /**
     * Wrapper on custom GATT service providing API to customer specific functionality.
     */
    var customServiceWrapper: CustomServiceWrapper? = null
        private set

    /**
     * Wrapper on OTA GATT service providing API to start, abort and monitor OTA progress.
     */
    var otaServiceWrapper: OTAServiceWrapper? = null
        private set

    /**
     * OTA functionality manager providing API for app clients.
     */
    var otaManager: OTAManager? = null
        private set

    /**
     * Indicates current global OTA state.
     */
    var isOTAInProgress: Boolean = false
        private set

    lateinit var systemBluetoothStateReceiver: SystemBluetoothStateReceiver
        private set

    private val myBinder = LocalBinder()
    private val gattHandler = GattHandler(this)
    private var connectionState: ConnectionState = ConnectionState.DISCONNECTED

    private var connectionStateDisposable: Disposable? = null
    private var readCharacteristicDisposable: Disposable? = null
    private var changedCharacteristicDisposable: Disposable? = null
    private var otaStateDisposable: Disposable? = null
    private var abConfigDisposable: Disposable? = null
    private var abEventsDisposable: Disposable? = null

    //endregion

    //region public functions

    override fun onCreate() {
        super.onCreate()
        systemBluetoothStateReceiver = SystemBluetoothStateReceiver(this)
        setupDeviceConnectionObserver()
    }

    override fun onBind(intent: Intent?): IBinder {
        return myBinder
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        customServiceWrapper?.writeSpeedMode(false)
        if (this::systemBluetoothStateReceiver.isInitialized) systemBluetoothStateReceiver.dispose()
        connectionStateDisposable?.dispose()
        readCharacteristicDisposable?.dispose()
        changedCharacteristicDisposable?.dispose()
        disposeDeviceSubscriptions()
        gattHandler.disconnectCurrentDevice()
        super.onTaskRemoved(rootIntent)
        stopSelf()
    }

    /**
     * Get currently connected/configured device.
     * @return current device instance reference.
     */
    fun getCurrentDevice(): AdidasBluetoothDevice {
        return device
    }

    /**
     * Establishes GATT connection to [device] BluetoothDevice to start getting data over BLE.
     * Method will try to connect to the device whenever it is available - autoConnect option is
     * set to true by default.
     */
    fun connect(device: AdidasBluetoothDevice) {
        //TODO: cover all API versions in this method.
        logd("connect")
        this.device = device
        gattHandler.connect(device)
    }

    /**
     * Cancel ongoing GATT connection for currently connected device.
     */
    fun disconnectCurrentDevice() {
        gattHandler.disconnectCurrentDevice()
    }

    /**
     * Notify with Observable when AdidasBluetoothDevice connection state is changed.
     * Notification will emit ConnectionState after connecting to a device with connect() function.
     * @return Observable for AdidasBluetoothDevice connection state changes.
     */
    fun notifyDeviceConnectionState(): Observable<ConnectionState> {
        return gattHandler.notifyDeviceConnectionState()
    }

    /**
     * Function starts OTA procedure and registers observers to handle OTA flow and states.
     */
    fun startOTA() {
        otaManager?.let { manager ->
            manager.startOTA()
            isOTAInProgress = true
        }
    }

    /**
     * Check if the [currentVersion] is older than FW version on the server and start OTA update if true.
     * FW based on the [deviceType] will be will be downloaded. OTA will be continued if
     * [isOTAInProgress] is true otherwise OTA will start from the beginning.
     */
    fun checkForNewFWVersion(currentVersion: String, deviceType: AdidasBluetoothDevice.Type) {
        otaManager?.checkForNewFWVersion(
                filesDir.path,
                currentVersion,
                deviceType,
                isOTAInProgress)
    }
    //endregion

    private fun setupDeviceConnectionObserver() {
        connectionStateDisposable = gattHandler.notifyDeviceConnectionState()
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { connectionState ->
                    this.connectionState = connectionState
                    if (connectionState == ConnectionState.CONNECTED) {
                        handleConnectedState()
                    } else if (connectionState == ConnectionState.DISCONNECTED) {
                        gattHandler.resetQueue()
                        otaManager?.handleDisconnected()
                        disposeDeviceSubscriptions()
                    }
                }
    }

    private fun handleConnectedState() {
        initServiceProviders()
        otaManager = OTAManager(this, otaServiceWrapper)
        setupObservers()
        if (isOTAInProgress) {
            otaManager?.restartOTA(getCurrentDevice().type)
        }
        customServiceWrapper?.readABConfig()
        customServiceWrapper?.registerABEventNotification()
        customServiceWrapper?.registerABConfigNotification()
        batteryServiceWrapper?.registerNotification(true)
    }

    private fun initServiceProviders() {
        batteryServiceWrapper = BatteryServiceWrapper(gattHandler)
        informationServiceWrapper = DeviceInformationServiceWrapper(gattHandler)
        customServiceWrapper = CustomServiceWrapper(gattHandler, getCurrentDevice().type)
        otaServiceWrapper = OTAServiceWrapper(gattHandler)
    }

    private fun setupObservers() {
        setupReadCharacteristicObserver()
        setupChangedCharacteristicObserver()
        setupOTAStateObserver()
        setupABObservers()
    }

    private fun setupReadCharacteristicObserver() {
        readCharacteristicDisposable?.let {
            return
        }
        readCharacteristicDisposable = gattHandler.notifyReadCharacteristic()
                .subscribe { characteristic ->
                    characteristic?.let {
                        when (it.uuid) {
                            CHARACTERISTIC_BATTERY_LEVEL ->
                                batteryServiceWrapper?.onCharacteristicRead(it)
                            CHARACTERISTIC_FIRMWARE_REVISION,
                            CHARACTERISTIC_MODEL_NAME,
                            CHARACTERISTIC_SERIAL_NUMBER,
                            CHARACTERISTIC_MANUFACTURER_NAME ->
                                informationServiceWrapper?.onCharacteristicRead(it)
                            CHARACTERISTIC_PAIRING_MODE,
                            CHARACTERISTIC_ACTION_BUTTON_CONFIG,
                            CHARACTERISTIC_ACTION_BUTTON_EVENT,
                            CHARACTERISTIC_SPEED_MODE,
                            CHARACTERISTIC_EQ ->
                                customServiceWrapper?.onCharacteristicRead(it)
                            CHARACTERISTIC_OTA_COMMAND,
                            CHARACTERISTIC_OTA_RESPONSE ->
                                otaServiceWrapper?.onCharacteristicRead(it)
                            else -> {
                                //nop
                            }
                        }
                    }
                }
    }

    private fun setupChangedCharacteristicObserver() {
        changedCharacteristicDisposable?.let {
            return
        }
        changedCharacteristicDisposable = gattHandler.notifyChangedCharacteristic()
                .subscribe { characteristic ->
                    characteristic.let {
                        when (it.uuid) {
                            CHARACTERISTIC_BATTERY_LEVEL ->
                                batteryServiceWrapper?.onCharacteristicChanged(it)
                            CHARACTERISTIC_FIRMWARE_REVISION,
                            CHARACTERISTIC_MODEL_NAME,
                            CHARACTERISTIC_SERIAL_NUMBER,
                            CHARACTERISTIC_MANUFACTURER_NAME ->
                                informationServiceWrapper?.onCharacteristicChanged(it)
                            CHARACTERISTIC_PAIRING_MODE,
                            CHARACTERISTIC_ACTION_BUTTON_CONFIG,
                            CHARACTERISTIC_ACTION_BUTTON_EVENT,
                            CHARACTERISTIC_SPEED_MODE,
                            CHARACTERISTIC_EQ ->
                                customServiceWrapper?.onCharacteristicChanged(it)
                            CHARACTERISTIC_OTA_COMMAND,
                            CHARACTERISTIC_OTA_RESPONSE ->
                                otaServiceWrapper?.onCharacteristicChanged(it)
                            else -> {
                                //nop
                            }
                        }
                    }
                }
    }

    private fun setupOTAStateObserver() {
        otaStateDisposable = otaManager?.notifyOTAState()
                ?.subscribe { otaState ->
                    when (otaState) {
                        OTAUpgradeState.STARTED -> isOTAInProgress = true
                        OTAUpgradeState.COMPLETED -> isOTAInProgress = false
                        else -> {
                            //nop
                        }
                    }
                }
    }

    private fun setupABObservers() {
        abConfigDisposable = customServiceWrapper?.notifyABConfigRead()
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    it?.let { abConfig ->
                        if (ABUtils.isSpotifyConfigured(abConfig.actions)) {
                            if (!spotifyAppRemoteController.isConnected()) {
                                spotifyAppRemoteController.connect()
                            }
                        } else {
                            spotifyAppRemoteController.disconnect()
                        }
                    }
                }

        abEventsDisposable = customServiceWrapper?.notifyABEvent()
                ?.subscribe { abEvent ->
                    if (ABUtils.isSpotifyAction(abEvent.actionId)) {
                        val spotifyConfig = CommonPreferences(this)
                                .getSpotifyABConfig(device.bluetoothDevice.address, abEvent.actionType)
                        Log.d("trg", "got a config $spotifyConfig")

                        // here we run actions basing on event type. if config on device is null,
                        // then nothing happens. we need to check for empty config and redirect
                        // to ab settings, otherwise we'll get non-functional buttons

                        spotifyConfig?.let { config ->
                            // todo remove playArtist and playAlbum methods and use play(uri)
                            when (abEvent.actionId) {
                                ABUtils.ActionId.CUSTOM_SPOTIFY_PLAYLIST ->
                                    spotifyAppRemoteController.play(config.uri)
                                ABUtils.ActionId.CUSTOM_SPOTIFY_ARTIST -> spotifyAppRemoteController.playArtist()
                                ABUtils.ActionId.CUSTOM_SPOTIFY_ALBUM -> spotifyAppRemoteController.playAlbum()
                            }
                        }
                    }
                }
    }

    private fun disposeDeviceSubscriptions() {
        otaStateDisposable?.dispose()
        abConfigDisposable?.dispose()
        abEventsDisposable?.dispose()
    }

    inner class LocalBinder : Binder() {
        fun getService(): BLEService {
            return this@BLEService
        }
    }

}