package com.zoundindustries.bt.connectionservice.api

import android.bluetooth.BluetoothDevice
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.zoundindustries.bt.connectionservice.model.device.ARNOLD_FEATURES
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice.ConnectionState
import com.zoundindustries.bt.connectionservice.model.device.FREEMAN_FEATURES
import com.zoundindustries.bt.connectionservice.service.BLEScanner
import com.zoundindustries.bt.connectionservice.service.BLEService
import com.zoundindustries.bt.connectionservice.utils.adidasApplication
import com.zoundindustries.bt.connectionservice.utils.logd
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * This class is a main API for the clients. It defines full set of functions to support BLE
 * connection establishment and management for the Adidas headphones products.
 *
 * @property context to provide scan results to the specific context of the client.
 * @constructor creates connection with the BLEService and BLEScanner instance to get the BLE scan
 * results.
 */
class ConnectionManager(private val context: Context) {

    //region properties
    var bleService: BLEService? = context.adidasApplication.bleService
    
    private val bleScanner: BLEScanner = BLEScanner(context)
    //endregion

    //region public functions
    /**
     * Starts scanning for all available Adidas BLE headphones.
     */
    fun startScan(deviceId: String? = null) {
        bleScanner.startScan(deviceId)
    }

    /**
     * Stops scanning for all available Adidas BLE headphones.
     */
    fun stopScan() {
        bleScanner.stopScan()
    }

    /**
     * Establishes GATT connection to [device] BluetoothDevice to start getting data over BLE.
     */
    fun connect(deviceId: String) {
        logd("connecting to id: $deviceId")
        bleScanner.getRemoteDeviceFromId(deviceId)?.let { device ->
            connect(device)
        }
    }

    /**
     * Establishes GATT connection to [device] BluetoothDevice to start getting data over BLE.
     */
    fun connect(device: AdidasBluetoothDevice) {
        logd("connecting to: ${device.bluetoothDevice.name}")
        bleService?.connect(device)
    }

    /**
     * Get currently connected/configured device.
     * @return current device instance reference.
     */
    fun getCurrentDevice(): AdidasBluetoothDevice? {
        return bleService?.getCurrentDevice()
    }

    /**
     * Cancel ongoing GATT connection for currently connected device.
     */
    fun disconnectCurrentDevice() {
        logd("disconnect current")
        bleService?.disconnectCurrentDevice()
    }

    /**
     * Get the full AdidasBluetoothDevice form the BLEScanner object based on [deviceId].
     * @return the requested device matching the passed [deviceId]
     */
    fun getRemoteDeviceFromId(deviceId: String?): AdidasBluetoothDevice? {
        return bleScanner.getRemoteDeviceFromId(deviceId)
    }

    /**
     * Get list of currently paired devices from the device.
     * @return paired devices.
     */
    fun getPairedDevices(): MutableSet<BluetoothDevice>? {
        return bleScanner.getPairedDevices()
    }

    /**
     * Check if the [feature] is supported for the device [type]. Features definitions are placed in
     * Features file.
     * @return flag indicating if the feature is supported for the Type.
     */
    fun isFeatureSupported(type: AdidasBluetoothDevice.Type, feature: Int): Boolean {
        return when (type) {
            AdidasBluetoothDevice.Type.ARNOLD -> ARNOLD_FEATURES and feature == feature
            AdidasBluetoothDevice.Type.FREEMAN -> FREEMAN_FEATURES and feature == feature
        }
    }

    /**
     * Return flag indicating Bluetooth enabled/disabled on the device.
     */
    fun isSystemBluetoothEnabled(): Boolean {
        return bleScanner.isSystemBluetoothEnabled()
    }

    /**
     * Notify with Observable when new device is found. Notification will emit AdidasBluetoothDevice
     * after starting scan with startScan function.
     * @return Observable for newly found device.
     */
    fun notifyNewDevice(): Observable<AdidasBluetoothDevice> {
        return bleScanner.newDevice
    }

    /**
     * Notify with Observable when AdidasBluetoothDevice connection state is changed.
     * Notification will emit ConnectionState after connecting to a device with connect() function.
     * @return Observable for AdidasBluetoothDevice connection state changes.
     */
    fun notifyDeviceConnectionState(): Observable<ConnectionState>? {
        return bleService?.notifyDeviceConnectionState()
    }

    /**
     * Notify with Observable when BLEService connection state is changed.
     * Notification will emit ConnectionState automatically after creating this object.
     * @return Observable for BLEService connection state changes.
     */
    fun notifyServiceConnectionState(): Observable<Boolean> {
        return context.adidasApplication.notifyServiceConnected()
    }

    /**
     * Notify with Observable when Bluetooth connection state is changed.
     * Notification will emit Boolean value:
     * false - Bluetooth is OFF on the device,
     * true - Bluetooth is ON on the device.
     * @return Observable for Bluetooth connection state changes.
     */
    fun notifySystemBluetoothState(): Observable<Boolean>? {
        return bleService?.systemBluetoothStateReceiver?.notifyBluetoothState()
    }

    /**
     * Start the OTA process.
     */
    fun startOTA() {
        //TODO: implement file download from the server
        bleService?.startOTA()
    }
    //endregion
}

