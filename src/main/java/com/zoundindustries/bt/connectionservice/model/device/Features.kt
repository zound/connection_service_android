package com.zoundindustries.bt.connectionservice.model.device

/**
 * This file contains all features definitions for all device types.
 * This should be extended for every new device type supported by the system.
 */

//Battery service
const val BATTERY = 1

//Device information service
const val INFO_MAN_NAME = 1 shl 1
const val INFO_MODEL_NUMBER = 1 shl 2
const val INFO_SERIAL_NUMBER = 1 shl 3
const val INFO_FW_REVISION = 1 shl 4
const val INFO_HW_REVISION = 1 shl 5

//Custom service
const val CUSTOM_PAIRING_STATUS = 1 shl 6
const val CUSTOM_PAIRING_MODE = 1 shl 7
const val CUSTOM_SOURCE_CONNECTIONS = 1 shl 8
const val CUSTOM_RENAME = 1 shl 9
const val CUSTOM_AUTO_POWER_OFF = 1 shl 10
const val CUSTOM_REMOTE_POWER_OFF = 1 shl 11
const val CUSTOM_MUTE_MIC = 1 shl 12
const val CUSTOM_VOLUME = 1 shl 13
const val CUSTOM_VOLUME_LIMIT = 1 shl 14
const val CUSTOM_AUDIO_CONTROL = 1 shl 15
const val CUSTOM_NOW_PLAYING = 1 shl 16
const val CUSTOM_UI_SOUNDS = 1 shl 17
const val CUSTOM_AB_EVENT = 1 shl 18
const val CUSTOM_AB_CONF = 1 shl 19
const val CUSTOM_AB_GA = 1 shl 20
const val CUSTOM_SPEED_MODE = 1 shl 21
const val CUSTOM_EQ = 1 shl 22

//Connectivity
const val CONNECTIVITY = 1 shl 23

const val ARNOLD_FEATURES =
        BATTERY or
                INFO_MAN_NAME or
                INFO_MODEL_NUMBER or
                INFO_SERIAL_NUMBER or
                INFO_FW_REVISION or
                INFO_HW_REVISION or
                CUSTOM_PAIRING_STATUS or
                CUSTOM_PAIRING_MODE or
                CUSTOM_SOURCE_CONNECTIONS or
                CUSTOM_RENAME or
                CUSTOM_AUTO_POWER_OFF or
                CUSTOM_REMOTE_POWER_OFF or
                CUSTOM_MUTE_MIC or
                CUSTOM_VOLUME or
                CUSTOM_VOLUME_LIMIT or
                CUSTOM_AUDIO_CONTROL or
                CUSTOM_NOW_PLAYING or
                CUSTOM_UI_SOUNDS or
                CUSTOM_AB_EVENT or
                CUSTOM_AB_CONF or
                CUSTOM_SPEED_MODE or
                CUSTOM_EQ or
                CONNECTIVITY

const val FREEMAN_FEATURES =
        BATTERY or
                INFO_MAN_NAME or
                INFO_MODEL_NUMBER or
                INFO_SERIAL_NUMBER or
                INFO_FW_REVISION or
                INFO_HW_REVISION or
                CUSTOM_PAIRING_STATUS or
                CUSTOM_PAIRING_MODE or
                CUSTOM_SOURCE_CONNECTIONS or
                CUSTOM_RENAME or
                CUSTOM_AUTO_POWER_OFF or
                CUSTOM_REMOTE_POWER_OFF or
                CUSTOM_MUTE_MIC or
                CUSTOM_VOLUME or
                CUSTOM_VOLUME_LIMIT or
                CUSTOM_AUDIO_CONTROL or
                CUSTOM_NOW_PLAYING or
                CUSTOM_UI_SOUNDS or
                CUSTOM_AB_EVENT or
                CUSTOM_AB_CONF or
                CUSTOM_SPEED_MODE or
                CUSTOM_EQ or
                CONNECTIVITY

