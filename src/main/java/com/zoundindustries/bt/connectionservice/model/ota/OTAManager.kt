package com.zoundindustries.bt.connectionservice.model.ota

import android.content.Context
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.ota.OTAServiceWrapper
import com.zoundindustries.bt.connectionservice.model.ota.file.FirmwareFileDownloader
import com.zoundindustries.bt.connectionservice.model.ota.file.UpdateFileManager
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAFramesHelper
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAFramesHelper.Companion.getResumePointFromResponse
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAPacket
import com.zoundindustries.bt.connectionservice.utils.SimplifiedTimer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

private const val DATA_TRANSFER_TIMEOUT: Long = 10000
private const val MAX_DATA_TRANSFER_TIMEOUT_COUNT = 3

/**
 * Enum reflecting device upgrade states. The state is valid until device acknowledgment is
 * received and it moved to the next state:
 * NOT_STARTED            -  OTA is not started all states are set to default values,
 * STARTED                - OTA is started, VM_CONNECT has been sent and data flow can start,
 * REGISTER_NOTIFICATIONS - Register OTA notifications. This is needed as a first call to get all
 *                          OTA events,
 * UPGRADE_SYNC           - Synchronize the file ans send MD5 checksum to the device,
 * UPGRADE_START          - Inform device that upgrade will start,
 * UPGRADE_START_DATA     - Inform device that file data is ready to be send,
 * UPGRADING              - File raw data is being transferred to the device. Packets are split into
 *                          chunks and bytes are send until last packet indication is set.
 * VALIDATING             - Device is validating the file that was uploaded,
 * TRANSFER_COMPLETE      - File is validated and file transfer is completed. Device is ready to be
 *                          flashed,
 * COMMITTING             - File is flashed to the device. This is the last step of the upgrade,
 * COMPLETED              - OTA has successfully completed.
 * ABORTED                - OTA has been aborted and should not be automatically restarted until
 *                          next app start.
 */
enum class OTAUpgradeState {
    NOT_STARTED,
    STARTED,
    REGISTER_NOTIFICATIONS,
    UPGRADE_SYNC,
    UPGRADE_START,
    UPGRADE_START_DATA,
    UPGRADING,
    VALIDATING,
    TRANSFER_COMPLETE,
    REBOOTING,
    COMMITTING,
    COMPLETED,
    ABORTED
}

/**
 * Class providing OTA functionality API. It uses BLEService to interact with OTA characteristics.
 *
 * @property otaServiceWrapper to access characteristics wrappers.
 *
 */
class OTAManager(val context: Context, private val otaServiceWrapper: OTAServiceWrapper?) {

    private val TAG = "OTAManager"

    private val otaState = BehaviorSubject.create<OTAUpgradeState>()
    private val otaProgress = BehaviorSubject.create<Float>()

    private var otaUpgradeState = OTAUpgradeState.NOT_STARTED
    private var resumePoint = ResumePoint.START
    private var transferDataRetryCount = 0
    private var firmwareFileDownloader: FirmwareFileDownloader? = null

    private var otaResponseDisposable: Disposable? = null
    private var otaProgressDisposable: Disposable? = null
    private var startOTADisposable: Disposable? = null

    private lateinit var dataTransferTimer: CountDownTimer
    private lateinit var otaUpgradeFileManager: UpdateFileManager

    init {
        setupDataTransferTimer()
    }

    //region public functions

    /**
     * Check if the [currentVersion] is older than FW version on the server and start OTA update if true.
     * FW based on the [deviceType] will be will be saved int [path]. OTA will be continued if
     * [isOTAInProgress] is true otherwise OTA will start from the beginning.
     */
    fun checkForNewFWVersion(path: String,
                             currentVersion: String,
                             deviceType: AdidasBluetoothDevice.Type,
                             isOTAInProgress: Boolean) {
        firmwareFileDownloader = FirmwareFileDownloader(context, path, currentVersion, deviceType)

        if (isOTAInProgress) {
            firmwareFileDownloader?.continueOTA()
        } else {
            firmwareFileDownloader?.checkForAvailableUpdate()
        }
        startOTADisposable = firmwareFileDownloader?.notifyStartOTA()
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    handleStartOTA(deviceType)
                }
    }

    /**
     * Function starts OTA procedure and registers observers to handle OTA flow and states.
     */
    fun startOTA() {
        initObservers()
        sendInitOTAPacket()
    }

    /**
     * Invoke flashing procedure of the already uploaded FW. Device will reboot after this call.
     */
    fun proceedToImageFlash() {
        setOtaUpgradeState(OTAUpgradeState.REBOOTING)
        otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getTransferCompletedResponse())
    }

    /**
     * Notify OTA progress via Observable with Float value.
     * The value is in 0-100% range.
     * @return Observable emitting progress value.
     */
    fun notifyOTAProgress(): Observable<Float> {
        return otaProgress
    }

    /**
     * Notify OTA state via Observable with Result value.
     * Result.success value is OTAUpgradeState type.
     * Result.failure value is AdidasThrowable type.
     * @return Observable emitting OTA state value.
     */
    fun notifyOTAState(): Observable<OTAUpgradeState> {
        return otaState
    }

    /**
     * Aborts ongoing OTA process. Stops any ongoing flow and sends Abort commands to the device.
     */
    fun abortOTA() {
        dataTransferTimer.cancel()
        otaUpgradeFileManager.resetOTAProgress()
        setOtaUpgradeState(OTAUpgradeState.ABORTED)
        otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getAbort())
    }

    /**
     * Handles device connected state if OTA is in progress and continue update after reboot.
     */
    fun restartOTA(deviceType: AdidasBluetoothDevice.Type) {
        firmwareFileDownloader?.otaFile?.let { file ->
            otaUpgradeFileManager = UpdateFileManager(UpdateFileManager.getBytesFromFile(file), deviceType)
            initObservers()
            sendInitOTAPacket()
        }
    }

    /**
     * Handles device disconnected state. Disposes OTA response registration.
     */
    fun handleDisconnected() {
        otaResponseDisposable?.dispose()
        otaProgressDisposable?.dispose()
    }
    //endregion

    private fun setupDataTransferTimer() {
        dataTransferTimer = object : SimplifiedTimer(DATA_TRANSFER_TIMEOUT) {
            override fun onFinish() {
                if (transferDataRetryCount == MAX_DATA_TRANSFER_TIMEOUT_COUNT) {
                    return
                }
                Log.d(TAG, "OTA response not received: restarting...")
                if (otaUpgradeState == OTAUpgradeState.UPGRADING) {
                    otaServiceWrapper?.writeOTACommand(
                            OTAPacket(
                                    OTACommands.COMMAND_VM_UPGRADE_DISCONNECT,
                                    ByteArray(0),
                                    UpgradeOperation.NO_OPERATION))

                    otaUpgradeFileManager.resetOTAProgress()
                    sendInitOTAPacket()
                    transferDataRetryCount = transferDataRetryCount.inc()
                }
            }
        }
    }

    private fun initObservers() {
        otaResponseDisposable = otaServiceWrapper?.notifyOTAResponseChanged()
                ?.subscribe { packet ->
                    handleOTAState(packet)
                }
        otaServiceWrapper?.registerOTAResponseNotification(true)
    }


    private fun sendInitOTAPacket() {
        setOtaUpgradeState(OTAUpgradeState.STARTED)
        otaServiceWrapper?.writeOTACommand(
                OTAPacket(
                        OTACommands.COMMAND_VM_UPGRADE_CONNECT,
                        ByteArray(0),
                        UpgradeOperation.NO_OPERATION))
    }

    private fun handleOTAState(otaPacket: OTAPacket) {
        Log.d(TAG, "Handle OTA message: CommandId: ${otaPacket.commandId} " +
                "Operation: ${otaPacket.operation}$ State: $otaUpgradeState")

        if (otaUpgradeState == OTAUpgradeState.ABORTED) {
            return
        }
        if (checkForError(otaPacket)) {
            abortOTA()
            return
        }
        when (otaUpgradeState) {
            OTAUpgradeState.STARTED -> handleStartedResponse(otaPacket)
            OTAUpgradeState.REGISTER_NOTIFICATIONS -> handleRegisterNotifyResponse(otaPacket)
            OTAUpgradeState.UPGRADE_SYNC -> handleUpgradeSyncResponse(otaPacket)
            OTAUpgradeState.UPGRADE_START -> handleUpgradeStartResponse(otaPacket)
            OTAUpgradeState.UPGRADE_START_DATA -> handleUpgradeStartDataResponse(otaPacket)
            OTAUpgradeState.UPGRADING -> handleUpgradingResponse(otaPacket)
            OTAUpgradeState.VALIDATING -> handleValidatingResponse(otaPacket)
            OTAUpgradeState.COMMITTING -> handleCommittingResponse(otaPacket)
            else -> {
                //nop
            }
        }
    }

    private fun handleStartedResponse(otaPacket: OTAPacket) {
        if (otaPacket.commandId == OTACommands.ACK_VM_UPGRADE) {
            setOtaUpgradeState(OTAUpgradeState.REGISTER_NOTIFICATIONS)

            otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getOTANotificationPacket(
                    OTACommands.COMMAND_REGISTER_NOTIFICATION,
                    OTACommands.VMU_PACKET, null))
        }
    }

    private fun handleRegisterNotifyResponse(otaPacket: OTAPacket) {
        if (otaPacket.commandId == OTACommands.ACK_REGISTER_NOTIFICATION) {
            setOtaUpgradeState(OTAUpgradeState.UPGRADE_SYNC)

            firmwareFileDownloader?.otaFile?.let { file ->
                otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getSyncRequest(file))
            }
        }
    }

    private fun handleUpgradeSyncResponse(otaPacket: OTAPacket) {
        if (otaPacket.commandId == OTACommands.UPGRADE_COMMAND_NOTIFICATION &&
                otaPacket.operation == UpgradeOperation.UPGRADE_SYNC_CFM.toInt()) {
            setOtaUpgradeState(OTAUpgradeState.UPGRADE_START)

            resumePoint = ResumePoint.values()[getResumePointFromResponse(otaPacket.payload)]

            otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getStartReq())
        }
    }

    private fun handleUpgradeStartResponse(otaPacket: OTAPacket) {
        if (otaPacket.commandId == OTACommands.UPGRADE_COMMAND_NOTIFICATION &&
                otaPacket.operation == UpgradeOperation.UPGRADE_START_CFM.toInt()) {
            when (resumePoint) {
                ResumePoint.START -> {
                    setOtaUpgradeState(OTAUpgradeState.UPGRADE_START_DATA)
                    otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getStartDataReq())
                }
                ResumePoint.PRE_VALIDATE -> {
                    setOtaUpgradeState(OTAUpgradeState.VALIDATING)
                    otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getValidationRequest())
                }
                ResumePoint.PRE_REBOOT -> {
                    setOtaUpgradeState(OTAUpgradeState.TRANSFER_COMPLETE)
                }
                ResumePoint.POST_REBOOT -> {
                    setOtaUpgradeState(OTAUpgradeState.COMMITTING)
                    otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getProceedToCommit())
                }
                ResumePoint.POST_COMMIT ->  {
                    setOtaUpgradeState(OTAUpgradeState.COMMITTING)
                    otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getCommitConfirmation())
                }
            }
        }
    }

    private fun handleUpgradeStartDataResponse(otaPacket: OTAPacket) {
        if (otaPacket.commandId == OTACommands.UPGRADE_COMMAND_NOTIFICATION &&
                otaPacket.operation == UpgradeOperation.UPGRADE_DATA_BYTES_REQ.toInt()) {
            setOtaUpgradeState(OTAUpgradeState.UPGRADING)
            handleDataReqPacket(otaUpgradeFileManager, otaPacket, otaServiceWrapper)
        }
    }

    private fun handleUpgradingResponse(otaPacket: OTAPacket) {
        /*We have 3 options here:
        1. We have sent all bytes from file and we go to validation
        2. We have not sent all bytes from the current chunk so we don't need next chunk size from the device yet
        3. We have sent all bytes for current chunk and we request another one
        In case we don't get next UPGRADE_DATA_BYTES OTA is restarted*/
        if (otaPacket.commandId == OTACommands.ACK_VM_UPGRADE_CONTROL ||
                otaPacket.operation == UpgradeOperation.UPGRADE_DATA_BYTES_REQ.toInt()) {
            if (otaUpgradeFileManager.isLastPacket) {
                dataTransferTimer.cancel()
                setOtaUpgradeState(OTAUpgradeState.VALIDATING)
                otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getValidationRequest())
            } else if (otaPacket.operation == UpgradeOperation.UPGRADE_DATA_BYTES_REQ.toInt()) {
                handleDataReqPacket(otaUpgradeFileManager, otaPacket, otaServiceWrapper)
                dataTransferTimer.cancel()
                transferDataRetryCount = 0
            } else if (otaPacket.commandId == OTACommands.ACK_VM_UPGRADE_CONTROL) {
                if (otaUpgradeFileManager.currentBytesToSendSize > 0) {
                    otaServiceWrapper?.writeOTACommand(otaUpgradeFileManager.getNextDataPacket())
                } else {
                    dataTransferTimer.start()
                }
            }
        }
    }

    private fun handleValidatingResponse(otaPacket: OTAPacket) {
        if (otaPacket.commandId == OTACommands.UPGRADE_COMMAND_NOTIFICATION) {
            when (otaPacket.operation) {
                UpgradeOperation.UPGRADE_IS_VALIDATION_DONE_CFM.toInt() -> {
                    Handler().postDelayed({
                        otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getValidationRequest())
                    },
                            OTAFramesHelper.getTimeResponse(otaPacket.payload))
                }
                UpgradeOperation.UPGRADE_TRANSFER_COMPLETE_IND.toInt() -> {
                    setOtaUpgradeState(OTAUpgradeState.TRANSFER_COMPLETE)
                }
            }
        }
    }

    private fun handleCommittingResponse(otaPacket: OTAPacket) {
        if (otaPacket.commandId == OTACommands.UPGRADE_COMMAND_NOTIFICATION) {
            when (otaPacket.operation) {
                UpgradeOperation.UPGRADE_COMMIT_REQ.toInt() -> {
                    otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getCommitConfirmation())
                }
                UpgradeOperation.UPGRADE_COMPLETE_IND.toInt() -> {
                    setOtaUpgradeState(OTAUpgradeState.COMPLETED)
                    Log.d(TAG, "OTA Completed")
                }
            }
        }
    }

    private fun handleStartOTA(deviceType: AdidasBluetoothDevice.Type) {
        firmwareFileDownloader?.otaFile?.let { file ->
            otaUpgradeFileManager =
                    UpdateFileManager(UpdateFileManager.getBytesFromFile(file), deviceType)
            otaProgressDisposable = otaUpgradeFileManager.notifyOTAProgress()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { progress ->

                        otaProgress.onNext(progress)
                    }
            startOTA()
        }
    }

    private fun setOtaUpgradeState(otaUpgradeState: OTAUpgradeState) {
        this.otaUpgradeState = otaUpgradeState
        otaState.onNext(otaUpgradeState)
    }

    private fun checkForError(otaPacket: OTAPacket): Boolean {
        if (otaPacket.commandId == OTACommands.UPGRADE_COMMAND_NOTIFICATION &&
                otaPacket.operation == UpgradeOperation.UPGRADE_ERROR_WARN_IND.toInt()) {
            otaServiceWrapper?.writeOTACommand(OTAFramesHelper.getErrorConfirmation(otaPacket))
            return true
        }
        return false
    }

    private fun handleDataReqPacket(otaUpgradeFileManager: UpdateFileManager,
                                    otaPacket: OTAPacket,
                                    otaServiceWrapper: OTAServiceWrapper?) {
        otaUpgradeFileManager.currentBytesToSendSize =
                OTAFramesHelper.getBytesToSendFromResponse(otaPacket.payload)
        validateBytesToSend(otaUpgradeFileManager,
                OTAFramesHelper.getFileOffsetFromResponse(otaPacket.payload))

        otaServiceWrapper?.writeOTACommand(otaUpgradeFileManager.getNextDataPacket())
    }

    private fun validateBytesToSend(otaUpgradeFileManager: UpdateFileManager, fileOffset: Int) {
        setBytesInFileOffset(otaUpgradeFileManager, fileOffset)
        setCurrentBytesToSendSize(otaUpgradeFileManager)
    }

    private fun setCurrentBytesToSendSize(fileMgr: UpdateFileManager) {
        val remainingLength = fileMgr.sourceFileBytes.size -
                fileMgr.bytesInFileOffset

        if (fileMgr.currentBytesToSendSize >= remainingLength) {
            fileMgr.currentBytesToSendSize = remainingLength
        } else if (fileMgr.currentBytesToSendSize <= 0) {
            fileMgr.currentBytesToSendSize = 0
        }
    }

    private fun setBytesInFileOffset(fileMgr: UpdateFileManager, fileOffset: Int) {
        if (fileOffset > 0 && fileOffset + fileMgr.bytesInFileOffset <
                fileMgr.sourceFileBytes.size) {
            fileMgr.bytesInFileOffset += fileOffset
        }
    }
}