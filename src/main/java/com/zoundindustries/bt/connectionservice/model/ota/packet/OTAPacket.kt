package com.zoundindustries.bt.connectionservice.model.ota.packet

import com.zoundindustries.bt.connectionservice.model.ota.OTACommands

private const val OFFSET_VENDOR_ID = 0
private const val LENGTH_VENDOR_ID = 2
private const val OFFSET_COMMAND_ID = 2
private const val LENGTH_COMMAND_ID = 2
private const val OFFSET_PAYLOAD = 4
private const val OFFSET_PACKET_HEADER = 8
private const val OFFSET_OPERATION = 5
private const val OFFSET_ERROR = 4

/**
 * Class wrapping requests and responses for the OTA commands. Class contains helper functions to
 * build and extract from raw commands data.
 *
 * @property commandId defining OTA command header,
 * @property payload raw data to be sent or received.
 * @property operation defines GAIA specific operation. OPerations are defined in UpgradeOperation.
 *
 */
class OTAPacket(val commandId: Int, val payload: ByteArray, val operation: Int) {

    constructor(response: ByteArray) :
            this(
                    getCommandIdFromResponse(response),
                    getPayloadFromResponse(response),
                    getOperationFromResponse(response))

    companion object {
        /**
         * Build raw ByteArray object containing data specific to OTA protocol based on [otaPacket].
         * @return raw ByteArray format to be send to device.
         */
        fun buildRequestFromOTAPacket(otaPacket: OTAPacket): ByteArray {
            if (otaPacket.payload.size > MAX_PAYLOAD) {
                //TODO: handle
            }

            val length = otaPacket.payload.size + OFFSET_PAYLOAD
            val bytes = ByteArray(length)

            //TODO: handle thrown errors
            OTAFramesHelper.copyIntIntoByteArray(
                    VENDOR_QUALCOMM,
                    bytes,
                    OFFSET_VENDOR_ID,
                    LENGTH_VENDOR_ID,
                    false)

            OTAFramesHelper.copyIntIntoByteArray(
                    otaPacket.commandId,
                    bytes,
                    OFFSET_COMMAND_ID,
                    LENGTH_COMMAND_ID,
                    false)

            System.arraycopy(otaPacket.payload,
                    0,
                    bytes,
                    OFFSET_PAYLOAD,
                    otaPacket.payload.size)

            return bytes
        }

        /**
         * Extract response from device response.
         * @return extracted response in ByteArray format.
         */
        private fun getPayloadFromResponse(response: ByteArray): ByteArray {
            //TODO: what if response too short
            return when {
                getCommandIdFromResponse(response) == OTACommands.UPGRADE_COMMAND_NOTIFICATION -> {
                    getDataFromResponse(response)
                }
                else -> ByteArray(0)
            }
        }

        private fun getOperationFromResponse(response: ByteArray): Int {
            return if (getCommandIdFromResponse(response) == OTACommands.UPGRADE_COMMAND_NOTIFICATION) {
                response[OFFSET_OPERATION].toInt()
            } else {
                0
            }
        }

        private fun getDataFromError(response: ByteArray): ByteArray {
            val dataLength = response.size - OFFSET_ERROR
            val result = ByteArray(dataLength)
            System.arraycopy(response, OFFSET_ERROR, result, 0, dataLength)
            return result
        }

        private fun getDataFromResponse(response: ByteArray): ByteArray {
            val dataLength = response.size - OFFSET_PACKET_HEADER
            val result = ByteArray(dataLength)
            System.arraycopy(response, OFFSET_PACKET_HEADER, result, 0, dataLength)
            return result
        }

        private fun getCommandIdFromResponse(response: ByteArray): Int {
            return OTAFramesHelper.extractIntFromByteArray(
                    response,
                    OFFSET_COMMAND_ID,
                    LENGTH_COMMAND_ID,
                    false)
        }
    }
}