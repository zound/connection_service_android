package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.ota

import android.util.Log
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAPacket

val CHARACTERISTIC_OTA_COMMAND = GattHandler.otaUUIDFromHeader("1101")

/**
 * Handles OTA Command read, write and notify on characteristic(0x1101).
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class OTACommandCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<OTAPacket>(
                gattHandler.characteristics[CHARACTERISTIC_OTA_COMMAND],
                gattHandler) {
    override fun processWriteValue(data: OTAPacket): ByteArray {
        val bytes = OTAPacket.buildRequestFromOTAPacket(data)
        logRequest(bytes)
        return bytes
    }

    override fun processNotifyValue(data: ByteArray): OTAPacket {
        return OTAPacket(data)
    }


    private fun logRequest(data: ByteArray) {
        var bytes = ""
        data.forEach {
            bytes += " ${String.format("0x%02X", it)}"
        }
        Log.d("OTAResponseProvider", " REQUEST:  $bytes")
    }
}