package com.zoundindustries.bt.connectionservice.model.spotify

import android.content.Context
import android.util.Log
import io.reactivex.subjects.BehaviorSubject
import com.spotify.android.appremote.api.ConnectionParams
import com.spotify.android.appremote.api.SpotifyAppRemote
import com.spotify.android.appremote.api.Connector.ConnectionListener

private const val TAG = "SpotifyAppRemoteCtrl"
private const val CLIENT_ID = "afa9c2011a5c4e268c6fa1f80eb6c5c9"
private const val REDIRECT_URI = "comzoundindustriesadidasheadphones://callback"

/**
 * Class handling the spotify communication
 */
class SpotifyAppRemoteController(val context: Context) {

    //region ------------------------  inputs  -----------------------------
    /**
     * Connect to the spotify service
     */
    fun connect() {
        SpotifyAppRemote.connect(context, connectionParams,
                object : ConnectionListener {
                    override fun onConnected(appRemote: SpotifyAppRemote) {
                        spotifyAppRemote = appRemote
                        spotifyAppRemote?.userApi?.capabilities?.setResultCallback {
                            canPlayOnDemand = it.canPlayOnDemand
                        }
                    }

                    override fun onFailure(throwable: Throwable) {
                        Log.e(TAG, throwable.message, throwable)
                    }
                })
    }

    /**
     * Disconnect from the spotify service, should be done on app exit
     */
    fun disconnect() {
        SpotifyAppRemote.disconnect(spotifyAppRemote)
    }

    /**
     * Play a hardcoded playlist
     */
    fun play(uri: String) {
        spotifyAppRemote?.playerApi?.play(uri)
    }

    /**
     * Play a hardcoded artist
     */
    fun playArtist() {
        spotifyAppRemote?.playerApi?.play("spotify:artist:1uiEZYehlNivdK3iQyAbye")
    }

    /**
     * Play a hardcoded album
     */
    fun playAlbum() {
        spotifyAppRemote?.playerApi?.play("spotify:album:2ZytN2cY4Zjrr9ukb2rqTP")
    }

    /**
     * Check if app remote is connected
     */
    fun isConnected(): Boolean {
        return spotifyAppRemote?.isConnected ?: false
    }
    //endregion
    //region ------------------------  outputs  -----------------------------
    /**
     * Notifies the state of spotify service connection
     */
    val notifyConnectionState: BehaviorSubject<Boolean> = BehaviorSubject.createDefault(false)

    /**
     * Reflect the state of user account. If it's true, then it's a premium account
     */
    var canPlayOnDemand : Boolean? = null
    //endregion

    private var spotifyAppRemote: SpotifyAppRemote? = null

    private val connectionParams: ConnectionParams? = ConnectionParams.Builder(CLIENT_ID)
            .showAuthView(true)
            .setRedirectUri(REDIRECT_URI)
            .build()

}
