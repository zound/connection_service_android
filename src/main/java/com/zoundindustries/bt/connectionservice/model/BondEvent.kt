package com.zoundindustries.bt.connectionservice.model

/**
 * Class wrapping bonding state changed to pass it to client(app) layer.
 *
 * @property address MAC address of the device that has changed the sate.
 * @property bondState new BT classic bond (pairing) state.
 */
data class BondEvent(val address: String, val bondState: Int)