package com.zoundindustries.bt.connectionservice.model.spotify

import android.util.Log
import com.zoundindustries.bt.connectionservice.utils.currentTimeSeconds

/**
 * Class wrapping the spotify Web API token with it's expiration timestamp
 * @param value token string
 * @param expirationTimestamp timestamp of token expiration moment
 */
class SpotifyToken(val value: String, val expirationTimestamp: Long) {
    fun isExpired(): Boolean {
        return (currentTimeSeconds() > expirationTimestamp)
    }
}

/**
 * Check if spotify web access token is not null or expired.
 * @param token checked value
 */
fun isSpotifyTokenValid(token: SpotifyToken?): Boolean {
    token?.let {
        if(token.value != "")
            return !it.isExpired()
    }
    return false
}