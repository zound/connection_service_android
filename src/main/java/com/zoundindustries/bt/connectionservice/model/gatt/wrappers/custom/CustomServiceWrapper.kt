package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import android.bluetooth.BluetoothGattCharacteristic
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import io.reactivex.Observable

/**
 * Custom service GATT service provider (0xAA00). This Service encapsulates all vendor specific
 * characteristics handling.
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class CustomServiceWrapper(private val gattHandler: GattHandler,
                           deviceType: AdidasBluetoothDevice.Type)
    : GattHandler.GattEventListener {

    //region properties
    private val pairingModeCharacteristicWrapper = PairingModeCharacteristicWrapper(gattHandler)
    private val abEventCharacteristicWrapper = ABEventCharacteristicWrapper(gattHandler)
    private val abConfigCharacteristicWrapper = ABConfigCharacteristicWrapper(gattHandler)
    private val speedModeCharacteristicWrapper = SpeedModeCharacteristicWrapper(gattHandler)
    private val eqCharacteristicWrapper = EQCharacteristicWrapper(gattHandler, deviceType)
    //endregion

    override fun onCharacteristicRead(characteristic: BluetoothGattCharacteristic) {
        when (characteristic.uuid) {
            CHARACTERISTIC_PAIRING_MODE ->
                pairingModeCharacteristicWrapper.onCharacteristicRead(characteristic)
            CHARACTERISTIC_ACTION_BUTTON_EVENT ->
                abEventCharacteristicWrapper.onCharacteristicRead(characteristic)
            CHARACTERISTIC_ACTION_BUTTON_CONFIG ->
                abConfigCharacteristicWrapper.onCharacteristicRead(characteristic)
            CHARACTERISTIC_SPEED_MODE ->
                speedModeCharacteristicWrapper.onCharacteristicRead(characteristic)
            CHARACTERISTIC_EQ ->
                eqCharacteristicWrapper.onCharacteristicRead(characteristic)
        }
    }

    override fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {
        when (characteristic.uuid) {
            CHARACTERISTIC_PAIRING_MODE ->
                pairingModeCharacteristicWrapper.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_ACTION_BUTTON_EVENT ->
                abEventCharacteristicWrapper.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_ACTION_BUTTON_CONFIG ->
                abConfigCharacteristicWrapper.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_SPEED_MODE ->
                speedModeCharacteristicWrapper.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_EQ ->
                eqCharacteristicWrapper.onCharacteristicChanged(characteristic)
        }
    }

    /**
     * Request to read pairing mode value.
     * The value will be notified asynchronously via notifyPairingModeChanged function as an
     * Observable value.
     */
    fun readPairingMode() {
        pairingModeCharacteristicWrapper.readValue()
    }

    /**
     * Get Observable with the pairing mode value changes.
     * @return Observable with the new pairing mode value.
     */
    fun notifyPairingModeChanged(): Observable<Boolean>? {
        return pairingModeCharacteristicWrapper.notifyValueChanged()
    }

    /**
     * Get Observable with the pairing mode value read.
     * @return Observable with the new pairing mode value.
     */
    fun notifyPairingModeRead(): Observable<Boolean>? {
        return pairingModeCharacteristicWrapper.notifyValueRead()
    }

    /**
     * Request to read OTA speed mode value.
     * The value will be notified asynchronously via notifySpeedModeChanged function as an
     * Observable value.
     */
    fun readSpeedMode() {
        speedModeCharacteristicWrapper.readValue()
    }

    /**
     * Get Observable with the OTA speed mode value changes.
     * @return Observable with the new OTA speed mode value.
     */
    fun notifySpeedModeChanged(): Observable<Boolean>? {
        return speedModeCharacteristicWrapper.notifyValueChanged()
    }

    /**
     * Get Observable with the OTA speed mode value read.
     * @return Observable with the new OTA speed mode value.
     */
    fun notifySpeedModeRead(): Observable<Boolean>? {
        return speedModeCharacteristicWrapper.notifyValueRead()
    }

    /**
     * Request to write [data] OTA speed mode configuration as Boolean.
     */
    fun writeSpeedMode(data: Boolean) {
        speedModeCharacteristicWrapper.writeValue(data)
    }

    /**
     * Register for notification of the pairing mode value.
     * The value will be notified asynchronously via notifyPairingModeChanged function as an
     * Observable value.
     */
    fun registerPairingModeNotification() {
        pairingModeCharacteristicWrapper.registerNotification(true)
    }

    /**
     * Unregister from notification of the pairing mode value.
     * Handle with care. This disables notifications for every listener.
     * Should be done only in service, when we're certain we won't need the notification.
     */
    fun unregisterPairingModeNotification() {
        pairingModeCharacteristicWrapper.registerNotification(false)
    }

    /**
     * Get Observable with the Action Button event when user presses it.
     * @return Observable with the Action Button event.
     */
    fun notifyABEvent(): Observable<ABEvent>? {
        return abEventCharacteristicWrapper.notifyValueChanged()
    }

    /**
     * Register for notification of the Action Button event.
     * The value will be notified asynchronously via notifyABEvent function as an
     * Observable value.
     */
    fun registerABEventNotification() {
        abEventCharacteristicWrapper.registerNotification(true)
    }

    /**
     * Unregister notification of the Action Button event.
     * Handle with care. This disables notifications for every listener.
     * Should be done only in service, when we're certain we won't need the notification.
     */
    fun unregisterABEventNotification() {
        abEventCharacteristicWrapper.registerNotification(false)
    }

    /**
     * Request to read action button configuration as ABConfig.
     * The value will be notified asynchronously via notifyPairingModeChanged function as an
     * Observable value.
     */
    fun readABConfig() {
        abConfigCharacteristicWrapper.readValue()
    }

    /**
     * Request to write [data] action button configuration as ABConfig.
     */
    fun writeABConfig(data: ABConfig) {
        abConfigCharacteristicWrapper.writeValue(data)
    }

    /**
     * Get Observable with the action button configuration as ABConfig was read.
     * @return Observable with the new action button configuration value.
     */
    fun notifyABConfigRead(): Observable<ABConfig>? {
        return abConfigCharacteristicWrapper.notifyValueRead()
    }

    /**
     * Get Observable with the action button configuration as ABConfig changes.
     * @return Observable with the new action button configuration value.
     */
    fun notifyABConfigChanged(): Observable<ABConfig>? {
        return abConfigCharacteristicWrapper.notifyValueChanged()
    }


    /**
     * Register for notification of the Action Button configuration.
     * The value will be notified asynchronously via notifyABConfigRead function as an
     * Observable value.
     */
    fun registerABConfigNotification() {
        abConfigCharacteristicWrapper.registerNotification(true)
    }

    /**
     * Unregister notification of the Action Button configuration.
     * Handle with care. This disables notifications for every listener.
     * Should be done only in service, when we're certain we won't need the notification.
     */
    fun unregisterABConfigNotification() {
        abConfigCharacteristicWrapper.registerNotification(false)
    }

    /**
     * Request to read equalizer configuration as EQConfig.
     * The value will be notified asynchronously via notifyEQConfigChanged function as an
     * Observable value.
     */
    fun readEQConfig() {
        eqCharacteristicWrapper.readValue()
    }

    /**
     * Request to write [data] equalizer configuration as EQConfig.
     */
    fun writeEQConfig(data: EQConfig) {
        eqCharacteristicWrapper.writeValue(data)
    }

    /**
     * Get Observable with the equalizer configuration as EQConfig was read.
     * @return Observable with the new equalizer configuration value.
     */
    fun notifyEQConfigRead(): Observable<EQConfig>? {
        return eqCharacteristicWrapper.notifyValueRead()
    }
}