package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.deviceinformation

import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler

val CHARACTERISTIC_MANUFACTURER_NAME = GattHandler.gattUUIDFromHeader("00002a29")

/**
 * Handles firmware revision characteristic(0x2A29)
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class ManufacturerNameCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<String>(gattHandler.characteristics[CHARACTERISTIC_MANUFACTURER_NAME],
                gattHandler) {
    override fun processWriteValue(data: String): ByteArray {
        return data.toByteArray()
    }

    override fun processNotifyValue(data: ByteArray): String {
        return String(data)
    }
}