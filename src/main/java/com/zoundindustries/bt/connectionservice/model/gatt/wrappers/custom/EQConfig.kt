package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

/**
 * Class wrapping configuration for Equalizer.
 *
 * @property bass defines the value for 160HZ frequency of Equalizer.
 * @property low defines the value for 400HZ frequency of Equalizer.
 * @property mid defines the value for 1KHZ frequency of Equalizer.
 * @property upper defines the value for 2.5KHZ frequency of Equalizer.
 * @property mid defines the value for 6.25KHZ frequency of Equalizer.
 */
data class EQConfig(val bass: Int, val low: Int, val mid: Int, val upper:Int, val treble: Int) {

    /**
     * Return ArrayList<Int> representation of the EQConfig frequencies values.
     */
    fun asArrayList(): ArrayList<Int> {
        return arrayListOf(bass, low, mid, upper, treble)
    }
}