package com.zoundindustries.bt.connectionservice.model.ota.file

/**
 * Class wrapping firmware meta data.
 *
 * @property version of the FW version of the Firmware file.
 * @property firmware containing meta data about the FW file.
 */
data class FWUpdateData(val version: String, val firmware: Firmware)