package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.batterylevel

import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper

val CHARACTERISTIC_BATTERY_LEVEL = GattHandler.gattUUIDFromHeader("2a19")

/**
 * Battery level GATT service provider(0x180F).
 * Handles battery level characteristic(0x2A19)
 * Characteristics provider is only one so implemented as this only class.
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class BatteryServiceWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<Int>(gattHandler.characteristics[CHARACTERISTIC_BATTERY_LEVEL],
                gattHandler) {

    override fun processWriteValue(data: Int): ByteArray {
        return ByteArray(data)
    }

    override fun processNotifyValue(data: ByteArray): Int {
        return if (data.isNotEmpty()) data[0].toInt() else 0
    }
}
