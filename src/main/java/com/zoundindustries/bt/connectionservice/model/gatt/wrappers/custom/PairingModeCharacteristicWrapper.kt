package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper

val CHARACTERISTIC_PAIRING_MODE = GattHandler.gattUUIDFromHeader("aa02")

/**
 * Handles bluetooth classing pairing mode characteristic(0xAA02)
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class PairingModeCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<Boolean>(gattHandler.characteristics[CHARACTERISTIC_PAIRING_MODE],
                gattHandler) {
    override fun processWriteValue(data: Boolean): ByteArray {
        return ByteArray(if (data) 1 else 0)
    }

    override fun processNotifyValue(data: ByteArray): Boolean {
        return data[0].toInt() == 1
    }
}
