package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils.Companion.getActionIdFromByte
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils.Companion.getActionTypeFromByte

val CHARACTERISTIC_ACTION_BUTTON_EVENT = GattHandler.gattUUIDFromHeader("aa13")

/**
 * Handles action button press event characteristic(0xAA13).
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class ABEventCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<ABEvent>(
                gattHandler.characteristics[CHARACTERISTIC_ACTION_BUTTON_EVENT],
                gattHandler) {
    override fun processWriteValue(data: ABEvent): ByteArray {
        return byteArrayOf(data.buttonId.toByte(),
                data.actionType.toByte(),
                data.actionId.toByte()
        )
    }

    @ExperimentalUnsignedTypes
    override fun processNotifyValue(data: ByteArray): ABEvent {
        return ABEvent(data[0].toInt(),
                getActionTypeFromByte(data[1].toUByte()),
                getActionIdFromByte(data[2].toUByte()))
    }
}