package com.zoundindustries.bt.connectionservice.model.ota.file

import android.content.Context
import android.os.Environment
import android.util.Log
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import io.reactivex.subjects.BehaviorSubject
import java.io.File
import java.io.FileReader

private const val IDENTITY_POOL_ID = "eu-west-2:83c6509a-3795-4666-a50c-4946d69ec51f"
private const val BUCKET_NAME = "com.zoundindustries.bt-fw-server"
private const val BUCKET_DEVICES_PATH = "devices/"
private const val BUCKET_DEV_PATH = "/dev/"
private const val BUCKET_META_DATA_PATH = "current-version"

private const val BIN_FW_FILE_PATH = "/ota"
private const val FW_META_DATA_FILE_NAME = "meta_data"
private const val FW_BIN_FILE_NAME = "upgrade.bin"

/**
 * Class wrapping server connection and files download.
 *
 * @property context used to access AWS TransferUtility.
 * @property path path where downloaded files will be saved.
 * @property currentVersion of the current device to check for the newer FW.
 * @property deviceType current device type used to get proper FW from the server.
 */
class FirmwareFileDownloader(private val context: Context,
                             private val path: String,
                             private val currentVersion: String,
                             private val deviceType: AdidasBluetoothDevice.Type?) {

    var otaFile: File? = null
        private set

    private val TAG = "FFDownload"
    private val startOTA = BehaviorSubject.create<Unit>()

    private var transferUtility: TransferUtility

    init {
        transferUtility = initTransferUtility(context)
    }

    //region public functions
    /**
     * Check for available new FW for the currently connected device.
     *
     */
    fun checkForAvailableUpdate() {

        val filepath = BUCKET_DEVICES_PATH +
                deviceType.toString().toLowerCase().capitalize() +
                BUCKET_DEV_PATH +
                BUCKET_META_DATA_PATH

        val metaDataFileObserver = transferUtility.download(
                BUCKET_NAME,
                filepath,
                File(path + BIN_FW_FILE_PATH + File.separator + FW_META_DATA_FILE_NAME))

        metaDataFileObserver.setTransferListener(
                SimplifiedTransferListener(
                        ::logMetaDataDownloadProgress,
                        ::onMetaDataFileDownloaded,
                        ::onDownloadFailed,
                        ::logOnDownloadError))
    }

    /**
     * Notify about OTA start request based on the new FW availability. It will be invoke when new
     * FW is available and downloaded.
     * @return Observable sending Unit event when OTA needs to be started.
     */
    fun notifyStartOTA(): BehaviorSubject<Unit> {
        return startOTA
    }

    /**
     * Continue OTA on already downloaded FW file.
     */
    fun continueOTA() {
        otaFile = File(path + BIN_FW_FILE_PATH + File.separator + FW_BIN_FILE_NAME)
        startOTA.onNext(Unit)
    }

    private fun handleOnNewFirmwareAvailable(transferUtility: TransferUtility,
                                             path: String?,
                                             data: FWUpdateData,
                                             deviceType: AdidasBluetoothDevice.Type?) {
        Log.d(TAG, "new currentVersion: ${data.version}")
        val filepath = getFWServerPath(deviceType, data)
        val binFileObserver = transferUtility.download(
                BUCKET_NAME,
                filepath,
                File(path + BIN_FW_FILE_PATH + File.separator + FW_BIN_FILE_NAME)
        )

        binFileObserver.setTransferListener(
                SimplifiedTransferListener(
                        ::logBinDownloadProgress,
                        ::onBinFileDownloaded,
                        ::onDownloadFailed,
                        ::logOnDownloadError))
    }

    private fun getFWServerPath(deviceType: AdidasBluetoothDevice.Type?, data: FWUpdateData): String {
        return BUCKET_DEVICES_PATH +
                deviceType.toString().toLowerCase().capitalize() +
                BUCKET_DEV_PATH +
                data.firmware.name
    }

    private fun initTransferUtility(context: Context): TransferUtility {
        val credentialsProvider = CognitoCachingCredentialsProvider(
                context,
                IDENTITY_POOL_ID,
                Regions.EU_WEST_2
        )

        return TransferUtility.builder()
                .s3Client(AmazonS3Client(credentialsProvider, Region.getRegion(Regions.EU_WEST_2)))
                .context(context)
                .build()
    }

    private fun getUpdateDataFromJson(file: File): FWUpdateData {
        val reader = JsonReader(FileReader(file))
        return Gson().fromJson(reader, FWUpdateData::class.java)
    }

    private fun onMetaDataFileDownloaded() {
        val data =
                getUpdateDataFromJson(File(
                        path +
                                BIN_FW_FILE_PATH +
                                File.separator + FW_META_DATA_FILE_NAME))
        if (data.version != currentVersion) handleOnNewFirmwareAvailable(
                transferUtility,
                path,
                data,
                deviceType)
    }

    private fun onBinFileDownloaded() {
        otaFile = File(path + BIN_FW_FILE_PATH + File.separator + FW_BIN_FILE_NAME)
        startOTA.onNext(Unit)
    }

    private fun onDownloadFailed() {
        Log.d(TAG, "download failed")
    }

    private fun logMetaDataDownloadProgress(progress: Long) {
        Log.d(TAG, "meta data download progress: $progress")
    }

    private fun logBinDownloadProgress(progress: Long) {
        Log.d(TAG, "bin download progress: $progress")
    }

    private fun logOnDownloadError(message: String?) {
        Log.d(TAG, "bin download error: $message")
    }
}