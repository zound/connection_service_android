package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

class ABUtils {

    companion object {
        /**
         * For now we have only one Action Button with the following ID
         */
        const val AB_ID = 0

        /**
         * Convert [byte] value to the corresponding ActionId.
         * @return actionId based on Byte value.
         */
        @ExperimentalUnsignedTypes
        fun getActionIdFromByte(byte: UByte): ActionId {
            return when (byte) {
                0x00.toUByte() -> ActionId.NO_ACTION
                0x01.toUByte() -> ActionId.DEFAULT_VA
                0x02.toUByte() -> ActionId.GOOGLE_VA
                0x03.toUByte() -> ActionId.EQUALIZER
                0x80.toUByte() -> ActionId.CUSTOM_SPOTIFY_PLAYLIST // protocol CUSTOM_1
                0x81.toUByte() -> ActionId.CUSTOM_SPOTIFY_ARTIST   // protocol CUSTOM_2
                0x82.toUByte() -> ActionId.CUSTOM_SPOTIFY_ALBUM    // protocol CUSTOM_3
                else -> ActionId.NO_ACTION
            }
        }

        /**
         * Convert [byte] value to the corresponding ActionId.
         * @return actionId based on Byte value.
         */
        @ExperimentalUnsignedTypes
        fun getActionTypeFromByte(byte: UByte): GestureType {
            return when (byte) {
                0x00.toUByte() -> GestureType.SINGLE_PRESS
                0x01.toUByte() -> GestureType.DOUBLE_PRESS
                0x02.toUByte() -> GestureType.LONG_PRESS
                0x04.toUByte() -> GestureType.VERY_LONG_PRESS
                else -> GestureType.NOT_DEFINED
            }
        }

        /**
         * Check if the configuration has some spotify actions
         */
        fun isSpotifyConfigured(actions: Array<ActionId>): Boolean {
            actions.forEach { action ->
                if (action == ActionId.CUSTOM_SPOTIFY_PLAYLIST
                        || action == ActionId.CUSTOM_SPOTIFY_ARTIST
                        || action == ActionId.CUSTOM_SPOTIFY_ALBUM)
                    return true
            }
            return false
        }

        /**
         * Check if current event contains a spotify action
         */
        fun isSpotifyAction(actionId: ActionId): Boolean {
            return actionId == ActionId.CUSTOM_SPOTIFY_PLAYLIST
                    || actionId == ActionId.CUSTOM_SPOTIFY_ARTIST
                    || actionId == ActionId.CUSTOM_SPOTIFY_ALBUM
        }
    }

    /**
     * Enum defining all possible press types for Action Buttons.
     */
    enum class GestureType {
        SINGLE_PRESS,
        DOUBLE_PRESS,
        LONG_PRESS,
        VERY_LONG_PRESS,
        NOT_DEFINED;

        /**
         * Convert current enum value to Byte.
         * @return Byte with int value.
         */
        fun toByte(): Byte {
            return this.ordinal.toByte()
        }
    }

    /**
     * Enum defining actions possible for the button press.
     * [1, 127] - fixed action range
     * [128, 255] - custom action range
     */
    enum class ActionId(val action: Int) {
        NO_ACTION(0),
        DEFAULT_VA(1),
        GOOGLE_VA(2),
        EQUALIZER(3),
        CUSTOM_SPOTIFY_PLAYLIST(128),
        CUSTOM_SPOTIFY_ARTIST(129),
        CUSTOM_SPOTIFY_ALBUM(130);

        /**
         * Convert current enum value to Byte.
         * @return Byte with int value.
         */
        fun toByte(): Byte {
            return this.action.toByte()
        }
    }
}