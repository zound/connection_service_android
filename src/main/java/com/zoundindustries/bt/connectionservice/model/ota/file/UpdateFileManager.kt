package com.zoundindustries.bt.connectionservice.model.ota.file

import android.util.Log
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.model.errors.AdidasThrowable
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAFramesHelper
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAPacket
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.io.*
import java.security.MessageDigest

/**
 * Maximum packet length for BLE data transmission.
 */
const val MAX_DATA_LENGTH_ARNOLD = 64

/**
 * Maximum packet length for BLE data transmission.
 */
const val MAX_DATA_LENGTH_FREEMAN = 23

/**
 * MD5 file read buffer size.
 */
const val MD5_READ_BUFFER = 1024

/**
 * Class wrapping file upload to the device. It keeps track of bytes to be sent and current offset
 * of the file.
 *
 * @property sourceFileBytes raw bytes taken from the file to be sent to the device.
 */
class UpdateFileManager(val sourceFileBytes: ByteArray,
                        deviceType: AdidasBluetoothDevice.Type) {

    //region properties

    private val progress: PublishSubject<Float> = PublishSubject.create()
    private val maxDataLength = when(deviceType) {
        AdidasBluetoothDevice.Type.ARNOLD -> MAX_DATA_LENGTH_ARNOLD
        AdidasBluetoothDevice.Type.FREEMAN -> MAX_DATA_LENGTH_FREEMAN
    }

    /**
     * Size of the data requested by the device. This value is decreased by the number of last
     * sent packet. The requested data may be sent in chunks of max size of MAX_DATA_LENGTH
     */
    var currentBytesToSendSize: Int = 0

    /**
     * Offset of the already sent data from the file to continue sending data from the last point.
     */
    var bytesInFileOffset: Int = 0

    var isLastPacket = false
    //endregion

    companion object {
        /**
         * Get raw ByteArray bytes from [file].
         * @return ByteArray with raw file bytes.
         */
        fun getBytesFromFile(file: File): ByteArray {
            val resultFileBytes: ByteArray

            try {
                val inputStream = BufferedInputStream(FileInputStream(file))
                val length = file.length().toInt()

                resultFileBytes = ByteArray(length)
                val bytesRead = inputStream.read(resultFileBytes)
                inputStream.close()

                return if (bytesRead == length || bytesRead == -1 && length == Integer.MAX_VALUE) {
                    resultFileBytes
                } else {
                    throw AdidasThrowable(AdidasThrowable.ErrorType.OTA_FAILED, "Wrong file size")
                }
            } catch (e: IOException) {
                //TODO: Think of throwing an error
                return ByteArray(0)
            }
        }

        /**
         * Obtains the MD5 checksum from a [file].
         * @return ByteArray with the MD5 checksum in form of bytes.
         */
        fun getMD5FromFile(file: File): ByteArray {
            var inputStream: InputStream? = null
            try {
                inputStream = FileInputStream(file)
                val buffer = ByteArray(MD5_READ_BUFFER)
                val digest = MessageDigest.getInstance("MD5")
                var numRead = 0

                while (numRead != -1) {
                    numRead = inputStream.read(buffer)
                    if (numRead > 0) {
                        digest.update(buffer, 0, numRead)
                    }
                }
                return digest.digest()
            } catch (e: Exception) {
                //TODO: think of throwing here
                return ByteArray(0)
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close()
                    } catch (e: Exception) {
                        //nop
                    }
                }
            }
        }
    }

    //region public functions
    /**
     * Gets next part of the file byte array and update the file data state.
     * @return OTAPacket with the next chunk of file data.
     */
    fun getNextDataPacket(): OTAPacket {
        Log.d("PROGRESS", "PROGRESS ----> " +
                "${((bytesInFileOffset.toFloat() / sourceFileBytes.size.toFloat()) * 100)}" +
                " ---> OFFSET: $bytesInFileOffset ---> SIZE: ${sourceFileBytes.size}")

        progress.onNext((((bytesInFileOffset.toFloat()) / sourceFileBytes.size.toFloat()) * 100))

        val bytesToSend = if (this.currentBytesToSendSize < maxDataLength - 1)
            this.currentBytesToSendSize else maxDataLength - 1

        val isLastPacket = sourceFileBytes.size - bytesInFileOffset <= bytesToSend
        val dataToSend = ByteArray(bytesToSend)

        System.arraycopy(sourceFileBytes, bytesInFileOffset, dataToSend, 0, dataToSend.size)

        if (isLastPacket) {
            this.isLastPacket = true
            this.currentBytesToSendSize = 0
        } else {
            this.bytesInFileOffset += bytesToSend
            this.currentBytesToSendSize -= bytesToSend
        }
        return OTAFramesHelper.getData(isLastPacket, dataToSend)
    }

    /**
     * Notify OTA progress via Observable with Float value.
     * The value is in 0-100% range.
     * @return Observable emitting progress value.
     */
    fun notifyOTAProgress(): Observable<Float> {
        return progress
    }

    /**
     * Reset file properties to start OTA from scratch.
     */
    fun resetOTAProgress() {
        currentBytesToSendSize = 0
        bytesInFileOffset = 0
        isLastPacket = false
    }
    //endregion
}