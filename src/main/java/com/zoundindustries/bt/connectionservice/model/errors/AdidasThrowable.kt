package com.zoundindustries.bt.connectionservice.model.errors

/**
 * Class wrapping Throwable class with Adidas specific error type.
 *
 * @property type defines unique error type form Adidas specific error types.
 * @property message additional description of the Adidas specific error.
 * @constructor creates instance of AdidasThrowable with the Adidas [type]and uses super class
 * to set the [message].
 */
class AdidasThrowable(val type: ErrorType, message: String) : Throwable(message) {

    /**
     * Definition of all possible Adidas specific errors.
     */
    enum class ErrorType {
        SCAN_NO_DEVICES_FOUND,
        SCAN_CURRENT_DEVICE_NOT_FOUND,
        SCAN_FAILED,
        OTA_FAILED
    }
}