package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper

val CHARACTERISTIC_SPEED_MODE = GattHandler.gattUUIDFromHeader("aa15")

/**
 * Handles OTA speed mode characteristic(0xAA15)
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class SpeedModeCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<Boolean>(gattHandler.characteristics[CHARACTERISTIC_SPEED_MODE],
                gattHandler) {
    override fun processWriteValue(data: Boolean): ByteArray {
        return byteArrayOf(if (data) 0x01 else 0x00)
    }

    override fun processNotifyValue(data: ByteArray): Boolean {
        return data[0].toInt() == 1
    }
}