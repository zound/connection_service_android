package com.zoundindustries.bt.connectionservice.model.spotify

import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils

/**
 * Class wrapping the map of spotify data for given gesture type
 */
class DeviceSpotifyConfig(val configMap: MutableMap<ABUtils.GestureType, SpotifyData> = mutableMapOf())

/**
 * Class containing spotify data common for all spotify action types
 */
class SpotifyData(val actionType: ABUtils.ActionId, val name: String, val uri: String) {
    override fun toString(): String {
        return "${actionType.name}: $name -> $uri"
    }
}
