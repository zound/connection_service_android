package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.deviceinformation

import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler

val CHARACTERISTIC_MODEL_NAME = GattHandler.gattUUIDFromHeader("00002a24")

/**
 * Handles model name characteristic(0x2A24)
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class ModelNameCharacteristicWrapper(private val gattHandler: GattHandler):
    BaseCharacteristicWrapper<String>(gattHandler.characteristics[CHARACTERISTIC_MODEL_NAME],
        gattHandler) {
        override fun processWriteValue(data: String): ByteArray {
            return data.toByteArray()
        }

        override fun processNotifyValue(data: ByteArray): String {
            return String(data)
        }
}