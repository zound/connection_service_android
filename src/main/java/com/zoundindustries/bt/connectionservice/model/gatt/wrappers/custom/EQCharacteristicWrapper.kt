package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper

val CHARACTERISTIC_EQ = GattHandler.gattUUIDFromHeader("aa16")

/**
 * Handles equalizer configuration characteristic(0xAA16)
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class EQCharacteristicWrapper(private val gattHandler: GattHandler,
                              private val deviceType: AdidasBluetoothDevice.Type) :
        BaseCharacteristicWrapper<EQConfig>(gattHandler.characteristics[CHARACTERISTIC_EQ],
                gattHandler) {
    override fun processWriteValue(data: EQConfig): ByteArray {
        val resultArray = ByteArray(if (deviceType == AdidasBluetoothDevice.Type.ARNOLD) 8 else 5)
        resultArray[0] = data.bass.toByte()
        resultArray[1] = data.low.toByte()
        resultArray[2] = data.mid.toByte()
        resultArray[3] = data.upper.toByte()
        resultArray[4] = data.treble.toByte()

        if (deviceType == AdidasBluetoothDevice.Type.ARNOLD) {
            resultArray[5] = 0x00
            resultArray[6] = 0x00
            resultArray[7] = 0x00
        }
        return resultArray
    }

    override fun processNotifyValue(data: ByteArray): EQConfig {
        val data = EQConfig(data[0].toInt(),
                data[1].toInt(),
                data[2].toInt(),
                data[3].toInt(),
                data[4].toInt())
        return data
    }
}