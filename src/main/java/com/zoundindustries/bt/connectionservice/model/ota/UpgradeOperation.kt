package com.zoundindustries.bt.connectionservice.model.ota

/**
 * Enum class defining all possible resume points of the upgrade. Resume point is used in
 * UPGRADE_START_CFM operation to continue OTA from last stored point.
 * START             - This is the resume point "0", that means the upgrade will start from the
 *                     beginning, the UPGRADE_START_DATA_REQ request.
 * PRE_VALIDATE      - This is the 1st resume point, that means the upgrade should resume from the
 *                     UPGRADE_IS_CSR_VALID_DONE_REQ request.
 * PRE_REBOOT        - This is the 2nd resume point, that means the upgrade should resume from the
 *                     UPGRADE_TRANSFER_COMPLETE_RES request.\
 * POST_REBOOT       - This is the 3rd resume point, that means the upgrade should resume from the
 *                     UPGRADE_IN_PROGRESS_RES request.
 * POST_COMMIT       - This is the 4th resume point, that means the upgrade should resume from the
 *                     UPGRADE_COMMIT_CFM confirmation request.
 */
enum class ResumePoint {
    START,
    PRE_VALIDATE,
    PRE_REBOOT,
    POST_REBOOT,
    POST_COMMIT
}

/**
 * Class defining type of operation for VM Upgrade commands.
 * Operation defines the actual upgrade command which is sent via VM Upgrade layer.
 */
class UpgradeOperation {

    companion object {

        const val NO_OPERATION = 0

        //region request operations
        /**
         * Request upgrade procedure to start.
         */
        const val UPGRADE_START_REQ: Byte = 0x01

        /**
         * Request to transfer sections of the upgrade image file to the board.
         */
        const val UPGRADE_DATA_REQ: Byte = 0x04

        /**
         * Request abort of the upgrade procedure
         */
        const val UPGRADE_ABORT_REQ: Byte = 0x07


        /**
         * To respond to the UPGRADE_TRANSFER_COMPLETE_IND UPGRADE_TRANSFER_COMPLETE_IND message.
         */
        const val UPGRADE_TRANSFER_COMPLETE_RESP: Byte = 0x0C

        /**
         * Proceed to commit the uploaded FW.
         */
        const val UPGRADE_PROCEED_TO_COMMIT: Byte = 0x0E
        /**
         * Used by the board to indicate it is ready for permission to commit the upgrade
         */
        const val UPGRADE_COMMIT_REQ: Byte = 0x0F

        /**
         * Request to synchronize with the board before any other protocol message.
         */
        const val UPGRADE_SYNC_REQ: Byte = 0x13

        /**
         * Used by the Host to request for executable partition validation status.
         */
        const val UPGRADE_IS_VALIDATION_DONE_REQ: Byte = 0x16


        //endregion

        //region response operations
        /**
         * Confirm start of the upgrade procedure.
         */
        const val UPGRADE_START_CFM: Byte = 0x02

        /**
         * Request the section of the upgrade image file bytes array expected by the board.
         */
        const val UPGRADE_DATA_BYTES_REQ: Byte = 0x03

        /**
         * To indicate the upgrade image file has successfully been received and validated.
         */
        const val UPGRADE_TRANSFER_COMPLETE_IND: Byte = 0x0B

        /**
         * Confirm firmware commit action
         */
        const val UPGRADE_COMMIT_CFM: Byte = 0x10

        /**
         * Indication to infrom the application that file commit to the device was
         * successfully done.
         */
        const val UPGRADE_COMPLETE_IND: Byte = 0x12

        /**
         * Confirm successful sync with the board.
         */
        const val UPGRADE_SYNC_CFM: Byte = 0x14

        /**
         * Indication to inform the application about errors or warnings.
         * Errors are considered as fatal.
         */
        const val UPGRADE_ERROR_WARN_IND: Byte = 0x11

        /**
         * request to start a data transfer.
         */
        const val UPGRADE_START_DATA_REQ: Byte = 0x15

        /**
         * Used by the Device to respond to the UPGRADE_IS_VALIDATION_DONE_REQ
         */
        const val UPGRADE_IS_VALIDATION_DONE_CFM: Byte = 0x17

        /**
         * Error response. Needs to be sent with the received data.
         */
        const val UPGRADE_ERROR_WARN_RESP: Byte = 0x1f
    }
}