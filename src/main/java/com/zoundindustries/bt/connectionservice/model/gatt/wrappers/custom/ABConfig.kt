package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils.ActionId

/**
 * Class wrapping configuration for Action Button.
 *
 * @property buttonId defines the ID of the button that is configured.
 * @property actions array of actions assigned to the button.
 */
data class ABConfig(val buttonId: Int,
                    val actions: Array<ActionId>) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ABConfig

        if (buttonId != other.buttonId) return false
        if (!actions.contentEquals(other.actions)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = buttonId
        result = 31 * result + actions.contentHashCode()
        return result
    }

}