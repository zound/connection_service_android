package com.zoundindustries.bt.connectionservice.model.ota.file

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState

/**
 * Class wrapping TransferListener providing API based on function parameters.
 *
 * @property onTransferProgress              function that will be called on transfer progress.
 *                                           It gets progress value parameter.
 * @property onTransferSuccessfullyCompleted function called when transfer is successfully completed
 * @property onTransferFailed                function called when transfer failed with noe
 *                                           additional message
 * @property onTransferError function called when there was an Exception during file transfer. It
 *                                           gets exception message as parameter.
 */
class SimplifiedTransferListener(private val onTransferProgress: (progress: Long) -> Unit,
                                 private val onTransferSuccessfullyCompleted: () -> Unit,
                                 private val onTransferFailed: () -> Unit,
                                 private val onTransferError: (String?) -> Unit) : TransferListener {

    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
        onTransferProgress(bytesCurrent / bytesTotal * 100)
    }

    override fun onStateChanged(id: Int, state: TransferState?) {
        when (state) {
            TransferState.COMPLETED -> onTransferSuccessfullyCompleted()
            TransferState.FAILED -> onTransferFailed
            else -> {
                //nop
            }
        }

    }

    override fun onError(id: Int, ex: Exception?) {
        onTransferError(ex?.message)
    }
}