package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils.ActionId
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils.GestureType

/**
 * Class wrapping Action Button event. Event is emitted when user presses the button.
 *
 * @property buttonId of the pressed button,
 * @property actionType what kind of press was performed,
 * @property actionId currently assigned to the button.
 */
data class ABEvent(val buttonId: Int,
                   val actionType: GestureType,
                   val actionId: ActionId)