package com.zoundindustries.bt.connectionservice.model.device

/**
 * Class mapping JSON structure for list of previously used devices.
 *
 * @property address bluetooth MAC Adress of the saved device,
 * @property type of the device from AdidasBluetoothDevice.Type.
 * @property name BT name of the device.
 */
class DeviceInfo(val address: String?, val type: AdidasBluetoothDevice.Type?, val name: String?) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DeviceInfo

        if (address != other.address) return false

        return true
    }

    override fun hashCode(): Int {
        return address?.hashCode() ?: 0
    }
}