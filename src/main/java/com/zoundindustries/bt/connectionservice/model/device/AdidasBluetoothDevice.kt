package com.zoundindustries.bt.connectionservice.model.device

import android.bluetooth.BluetoothDevice

/**
 * Class defining Adidas specific device object containing vendor specific Type and generic
 * BluetoothDevice object.
 *
 * @property bluetoothDevice is an Android specific bluetooth object for communicating with the
 * BT layer.
 * @property type is a vendor specific device value used to define specific model features.
 * @constructor creates the device to be used for GATT connection and BLE characteristics read,
 * write and notify.
 */
data class AdidasBluetoothDevice(val bluetoothDevice: BluetoothDevice, val type: Type) {

    /**
     * Enum class defining all possible types for Adidas headphones family supported by this
     * library.
     */
    enum class Type(val value: Int) {
        ARNOLD(0),
        FREEMAN(1);

        companion object {

            /**
             * Get Type instance from Int [value].
             */
            fun fromInt(value: Int) = Type.values().first { it.value == value }
        }
    }

    /**
     * Enum class defining all possible connection states for the AdidasBluetoothDevice.
     * DISCONNECTED - device is not connected, there was no connection attempt or connection failed.
     * CONNECTING   - GATT connection is being established and characteristics are being evaluated.
     *                Can be interrupted and go back to DISCONNECTED state.
     * CONNECTED    - device is connected to GATT server and all characteristics are evaluated.
     *                Device can be disconnected and go back to DISCONNECTED state.
     *
     */
    enum class ConnectionState {
        DISCONNECTED,
        CONNECTING,
        CONNECTED
    }
}