package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.deviceinformation

import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler

val CHARACTERISTIC_SERIAL_NUMBER = GattHandler.gattUUIDFromHeader("00002a25")

/**
 * Handles serial number characteristic(0x2A25)
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class SerialNumberCharacteristicWrapper(private val gattHandler: GattHandler):
    BaseCharacteristicWrapper<String>(gattHandler.characteristics[CHARACTERISTIC_SERIAL_NUMBER],
    gattHandler) {
        override fun processWriteValue(data: String): ByteArray {
            return data.toByteArray()
        }

        override fun processNotifyValue(data: ByteArray): String {
            return String(data)
        }
}