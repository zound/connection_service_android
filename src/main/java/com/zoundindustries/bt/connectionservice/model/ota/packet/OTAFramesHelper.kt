package com.zoundindustries.bt.connectionservice.model.ota.packet

import com.zoundindustries.bt.connectionservice.model.ota.OTACommands
import com.zoundindustries.bt.connectionservice.model.ota.UpgradeOperation
import com.zoundindustries.bt.connectionservice.model.ota.file.UpdateFileManager
import java.io.File

const val MAX_PAYLOAD = 16
const val VENDOR_QUALCOMM = 0x000A

private const val BYTES_IN_INT = 4
private const val BYTES_IN_SHORT = 2
private const val BYTES_IN_LONG = 8
private const val BITS_IN_BYTE = 8
private const val MD5_REQUEST_IDENTIFIER_LENGTH = 4
private const val UPGRADE_REQUEST_HEADER_LENGTH = 3
private const val SYNC_REQUEST_DATA_LENGTH =
        MD5_REQUEST_IDENTIFIER_LENGTH + UPGRADE_REQUEST_HEADER_LENGTH
private const val BYTES_TO_SEND_OFFSET = 0
private const val BYTES_TO_SEND_LENGTH = 4
private const val FILE_OFFSET_OFFSET = 4
private const val FILE_OFFSET_LENGTH = 4
private const val ERROR_DATA_OFFSET = 0
private const val WAITING_TIME_OFFSET = 0
private const val WAITING_TIME_LENGTH = 2
private const val RESUME_POINT_OFFSET = 0
private const val RESUME_POINT_LENGTH = 1
private const val START_STATUS_OFFSET = 0
private const val START_STATUS_LENGTH = 1

private const val COMMANDS_NOTIFICATION_MASK = 0x4000
private const val CONVERSION_MASK = 0xFF
private const val CONTINUE_UPGRADE: Byte = 0x01
private const val EMPTY_BYTE: Byte = 0x00

/**
 * Class providing utility functions to build and parse OTA data frames.
 */
class OTAFramesHelper {
    companion object {
        //region generic types helper functions
        /**
         * Converts [sourceValue] into [target] with the given offset [targetOffset] and of a given
         * [length]. Function can put [sourceValue] in a reversed order using [reverse] flag.
         */
        fun copyIntIntoByteArray(sourceValue: Int,
                                 target: ByteArray,
                                 targetOffset: Int,
                                 length: Int,
                                 reverse: Boolean) {
            if ((length < 0) or (length > BYTES_IN_INT)) {
                throw IndexOutOfBoundsException("Length must be between 0 and $BYTES_IN_INT")
            } else if (target.size < targetOffset + length) {
                throw IndexOutOfBoundsException("The targeted location must be contained in the target array.")
            }

            if (reverse) {
                var shift = 0
                for ((j, _) in (length - 1 downTo 0).withIndex()) {
                    val mask = CONVERSION_MASK shl shift
                    target[j + targetOffset] = (sourceValue and mask shr shift).toByte()
                    shift += BITS_IN_BYTE
                }
            } else {
                var shift = (length - 1) * BITS_IN_BYTE
                for (i in 0 until length) {
                    val mask = CONVERSION_MASK shl shift
                    target[i + targetOffset] = (sourceValue and mask shr shift).toByte()
                    shift -= BITS_IN_BYTE
                }
            }
        }

        /**
         * Extracts Int value from  [source] ByteArray with the given [offset] and of a given
         * [length]. Function can get the value in a reversed order using [reverse] flag.
         * @return Int value from the given bytes.
         */
        fun extractIntFromByteArray(source: ByteArray,
                                    offset: Int,
                                    length: Int,
                                    reverse: Boolean): Int {
            if ((length < 0) or (length > BYTES_IN_INT))
                throw IndexOutOfBoundsException("Length must be between 0 and $BYTES_IN_INT")
            var result = 0
            var shift = (length - 1) * BITS_IN_BYTE

            if (reverse) {
                for (i in offset + length - 1 downTo offset) {
                    result = result or (source[i].toInt() and CONVERSION_MASK shl shift)
                    shift -= BITS_IN_BYTE
                }
            } else {
                for (i in offset until offset + length) {
                    result = result or (source[i].toInt() and CONVERSION_MASK shl shift)
                    shift -= BITS_IN_BYTE
                }
            }
            return result
        }

        /**
         * Extracts Short value from  [source] ByteArray with the given [offset] and of a given
         * [length]. Function can get the value in a reversed order using [reverse] flag.
         * @return Short value from the given bytes.
         */
        fun extractShortFromByteArray(source: ByteArray,
                                      offset: Int,
                                      length: Int,
                                      reverse: Boolean): Short {
            if ((length < 0) or (length > BYTES_IN_SHORT))
                throw IndexOutOfBoundsException("Length must be between 0 and $BYTES_IN_SHORT")
            var result: Short = 0
            var shift = (length - 1) * BITS_IN_BYTE

            if (reverse) {
                for (i in offset + length - 1 downTo offset) {
                    result = (result.toInt() or (source[i].toInt() and CONVERSION_MASK shl shift))
                            .toShort()
                    shift -= BITS_IN_BYTE
                }
            } else {
                for (i in offset until offset + length) {
                    result = (result.toInt() or (source[i].toInt() and CONVERSION_MASK shl shift)
                            ).toShort()
                    shift -= BITS_IN_BYTE
                }
            }
            return result
        }

        /**
         * Extracts Long value from  [source] ByteArray with the given [offset] and of a given
         * [length]. Function can get the value in a reversed order using [reverse] flag.
         * @return Long value from the given bytes.
         */
        fun extractLongFromByteArray(source: ByteArray, offset: Int, length: Int, reverse: Boolean):
                Long {
            if ((length < 0) or (length > BYTES_IN_LONG))
                throw IndexOutOfBoundsException("Length must be between 0 and $BYTES_IN_LONG")
            var result: Long = 0
            var shift = (length - 1) * BITS_IN_BYTE

            if (reverse) {
                for (i in offset + length - 1 downTo offset) {
                    result = result or (source[i].toInt() and CONVERSION_MASK shl shift).toLong()
                    shift -= BITS_IN_BYTE
                }
            } else {
                for (i in offset until offset + length) {
                    result = result or (source[i].toInt() and CONVERSION_MASK shl shift).toLong()
                    shift -= BITS_IN_BYTE
                }
            }
            return result
        }
        //endregion

        //region create request type functions
        /**
         * Create OTA Notification OTAPacked from the given [commandID], [event] and [data].
         * @return OTAPacket used to write data to characteristic.
         */
        fun getOTANotificationPacket(commandID: Int,
                                     event: Int,
                                     data: ByteArray?): OTAPacket {
            if (commandID and COMMANDS_NOTIFICATION_MASK != COMMANDS_NOTIFICATION_MASK) {
                throw IllegalStateException("Wrong OTA command")
            }
            val eventOffset = 0
            val eventLength = 1
            val dataOffset = eventOffset + eventLength
            val payload: ByteArray

            if (data != null) {
                payload = ByteArray(eventLength + data.size)
                payload[eventOffset] = event.toByte()
                System.arraycopy(data, 0, payload, dataOffset, data.size)
            } else {
                payload = ByteArray(eventLength)
                payload[eventOffset] = event.toByte()
            }
            return OTAPacket(
                    commandID,
                    payload,
                    COMMANDS_NOTIFICATION_MASK)
        }

        /**
         * Create OTA Synchronization Request OTAPacked from the given [file].
         * @return OTAPacket used to write data to characteristic.
         */
        fun getSyncRequest(file: File): OTAPacket {
            // send the MD5 information here
            val md5Checksum = UpdateFileManager.getMD5FromFile(file)
            val identifierLength = MD5_REQUEST_IDENTIFIER_LENGTH
            val data = ByteArray(SYNC_REQUEST_DATA_LENGTH)
            val syncRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_SYNC_REQ,
                    EMPTY_BYTE,
                    MD5_REQUEST_IDENTIFIER_LENGTH.toByte())

            System.arraycopy(syncRequest, 0, data, 0, syncRequest.size)

            if (md5Checksum.size >= identifierLength) {
                System.arraycopy(md5Checksum,
                        md5Checksum.size - identifierLength,
                        data,
                        syncRequest.size,
                        identifierLength)
            } else if (md5Checksum.isNotEmpty()) {
                System.arraycopy(md5Checksum,
                        0,
                        data,
                        syncRequest.size,
                        md5Checksum.size)
            }
            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    data,
                    UpgradeOperation.UPGRADE_SYNC_REQ.toInt())
        }

        /**
         * Create OTA Start Request OTAPacked.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getStartReq(): OTAPacket {
            val startRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_START_REQ,
                    EMPTY_BYTE,
                    EMPTY_BYTE)

            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    startRequest,
                    UpgradeOperation.UPGRADE_START_REQ.toInt())
        }

        /**
         * Create OTA Start Data Request OTAPacked.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getStartDataReq(): OTAPacket {
            val startDataRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_START_DATA_REQ,
                    EMPTY_BYTE,
                    EMPTY_BYTE)

            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    startDataRequest,
                    UpgradeOperation.UPGRADE_START_DATA_REQ.toInt())
        }

        /**
         * Create OTA Start Request OTAPacked.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getValidationRequest(): OTAPacket {
            val startRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_IS_VALIDATION_DONE_REQ,
                    EMPTY_BYTE,
                    EMPTY_BYTE)

            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    startRequest,
                    UpgradeOperation.UPGRADE_IS_VALIDATION_DONE_REQ.toInt())
        }

        /**
         * Create OTA Data Request OTAPacked from the given [data] and indicate if data is the
         * last packet of file using [isLastPacket] flag.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getData(isLastPacket: Boolean, data: ByteArray): OTAPacket {
            val dataRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_DATA_REQ,
                    EMPTY_BYTE,
                    (data.size + 1).toByte(),
                    if (isLastPacket) 0x01 else 0x00)
            val dataToSend = ByteArray(data.size + dataRequest.size)

            System.arraycopy(dataRequest, 0, dataToSend, 0, dataRequest.size)
            System.arraycopy(data, 0, dataToSend, dataRequest.size, data.size)

            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    dataToSend,
                    UpgradeOperation.UPGRADE_DATA_REQ.toInt())
        }

        /**
         * Create OTA Transfer Completed response to continue to first device reboot after FW
         * validation.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getTransferCompletedResponse(): OTAPacket {
            val startRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_TRANSFER_COMPLETE_RESP,
                    EMPTY_BYTE,
                    EMPTY_BYTE,
                    EMPTY_BYTE)

            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    startRequest,
                    UpgradeOperation.UPGRADE_TRANSFER_COMPLETE_RESP.toInt())
        }

        /**
         * Create OTA Commit Confirmation response to proceed to newly uploaded FW flashing.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getCommitConfirmation(): OTAPacket {
            //TODO: CONTINUE or ABORT
            val startRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_COMMIT_CFM,
                    EMPTY_BYTE,
                    CONTINUE_UPGRADE,
                    EMPTY_BYTE)

            return OTAPacket(OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    startRequest,
                    UpgradeOperation.UPGRADE_COMMIT_CFM.toInt())
        }

        /**
         * Create OTA Commit Confirmation response to proceed to newly uploaded FW flashing.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getProceedToCommit(): OTAPacket {
            //TODO: CONTINUE or ABORT
            val startRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_PROCEED_TO_COMMIT,
                    EMPTY_BYTE,
                    CONTINUE_UPGRADE,
                    EMPTY_BYTE)

            return OTAPacket(OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    startRequest,
                    UpgradeOperation.UPGRADE_COMMIT_CFM.toInt())
        }

        /**
         * Extract number of bytes requested by the device via UPGRADE_DATA_BYTES_REQ form
         * the OTAPacket payload.
         * @return Int representation of the number of bytes to send.
         */
        fun getBytesToSendFromResponse(data: ByteArray): Int {
            return OTAFramesHelper.extractIntFromByteArray(
                    data,
                    BYTES_TO_SEND_OFFSET,
                    BYTES_TO_SEND_LENGTH,
                    false)
        }

        /**
         * Extract file offset for sending bytes form the OTAPacket payload.
         * @return Int representation of the file offset.
         */
        fun getFileOffsetFromResponse(data: ByteArray): Int {
            return OTAFramesHelper.extractIntFromByteArray(
                    data,
                    FILE_OFFSET_OFFSET,
                    FILE_OFFSET_LENGTH,
                    false)
        }

        /**
         * Get time value from the response from UPGRADE_IS_VALIDATION_DONE_CFM [data].
         * @return Long value with the time.
         */
        fun getTimeResponse(data: ByteArray): Long {
            return extractLongFromByteArray(data,
                    WAITING_TIME_OFFSET,
                    WAITING_TIME_LENGTH,
                    false)
        }

        /**
         * Get time value from the response from UPGRADE_IS_VALIDATION_DONE_CFM [data].
         * @return Long value with the time.
         */
        fun getResumePointFromResponse(data: ByteArray): Int {
            return extractIntFromByteArray(data,
                    RESUME_POINT_OFFSET,
                    RESUME_POINT_LENGTH,
                    false)
        }

        /**
         * Get time value from the response from UPGRADE_IS_VALIDATION_DONE_CFM [data].
         * @return Long value with the time.
         */
        fun getStatusFromResponse(data: ByteArray): Int {
            return extractIntFromByteArray(data,
                    START_STATUS_OFFSET,
                    START_STATUS_LENGTH,
                    false)
        }

        /**
         * Create Abort Request OTAPacked.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getAbort(): OTAPacket {
            val abortDataRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_ABORT_REQ,
                    EMPTY_BYTE,
                    EMPTY_BYTE)

            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    abortDataRequest,
                    UpgradeOperation.UPGRADE_ABORT_REQ.toInt())
        }

        /**
         * Create Abort Request OTAPacked.
         * @return OTAPacket used to write data to characteristic.
         */
        fun getErrorConfirmation(otaPacket: OTAPacket): OTAPacket {
            val errorConfirmationRequest = byteArrayOf(
                    UpgradeOperation.UPGRADE_ERROR_WARN_RESP,
                    EMPTY_BYTE,
                    (otaPacket.payload.size - ERROR_DATA_OFFSET).toByte())

            val dataToSend = ByteArray(
                    errorConfirmationRequest.size +
                            otaPacket.payload.size -
                            ERROR_DATA_OFFSET)

            System.arraycopy(
                    errorConfirmationRequest,
                    0,
                    dataToSend,
                    0,
                    errorConfirmationRequest.size)

            System.arraycopy(
                    otaPacket.payload,
                    ERROR_DATA_OFFSET,
                    dataToSend,
                    errorConfirmationRequest.size,
                    otaPacket.payload.size - ERROR_DATA_OFFSET)

            return OTAPacket(
                    OTACommands.COMMAND_VM_UPGRADE_CONTROL,
                    dataToSend,
                    UpgradeOperation.UPGRADE_ERROR_WARN_RESP.toInt())
        }
        //endregion
    }
}