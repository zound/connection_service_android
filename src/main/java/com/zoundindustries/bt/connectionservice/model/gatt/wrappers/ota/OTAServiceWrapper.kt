package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.ota

import android.bluetooth.BluetoothGattCharacteristic
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAPacket
import io.reactivex.Observable

/**
 * OTA service GATT service provider (00001100-d102-11e1-9b23-00025b00a5a5).
 * This Service encapsulates all OTA specific characteristics handling.
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class OTAServiceWrapper(private val gattHandler: GattHandler) : GattHandler.GattEventListener {

    //region properties
    private val otaCommandCharacteristicWrapper = OTACommandCharacteristicWrapper(gattHandler)
    private val otaResponseCharacteristicWrapper = OTAResponseCharacteristicWrapper(gattHandler)
    //endregion

    //region public functions
    override fun onCharacteristicRead(characteristic: BluetoothGattCharacteristic) {
        when (characteristic.uuid) {
            CHARACTERISTIC_OTA_COMMAND -> otaCommandCharacteristicWrapper.onCharacteristicRead(characteristic)
            CHARACTERISTIC_OTA_RESPONSE -> otaResponseCharacteristicWrapper.onCharacteristicRead(characteristic)
        }

    }

    override fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {
        when (characteristic.uuid) {
            CHARACTERISTIC_OTA_COMMAND -> otaCommandCharacteristicWrapper.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_OTA_RESPONSE -> otaResponseCharacteristicWrapper.onCharacteristicChanged(characteristic)
        }
    }

    /**
     * Request to read OTA Command value.
     * The value will be notified asynchronously via notifyOTACommandChanged function as an
     * Observable value.
     */
    fun readOTACommand() {
        otaCommandCharacteristicWrapper.readValue()
    }

    /**
     * Request to write OTA command value.
     */
    fun writeOTACommand(data: OTAPacket) {
        otaCommandCharacteristicWrapper.writeValue(data)
    }

    /**
     * Register OTA commands notification with [register] flag to turn it on or off.
     */
    fun registerOTACommandNotification(register: Boolean) {
        otaCommandCharacteristicWrapper.registerNotification(register)
    }

    /**
     * Request to read OTA Response value.
     * The value will be notified asynchronously via notifyOTAResponseChanged function as an
     * Observable value.
     */
    fun readOTAResponse() {
        otaResponseCharacteristicWrapper.readValue()
    }

    /**
     * Request to write OTA response value.
     */
    fun writeOTAResponse(data: OTAPacket) {
        otaResponseCharacteristicWrapper.writeValue(data)
    }

    /**
     * Register OTA response notification with [register] flag to turn it on or off.
     */
    fun registerOTAResponseNotification(register: Boolean) {
        otaResponseCharacteristicWrapper.registerNotification(register)
    }

    /**
     * Get Observable with the OTA Response value changes.
     * @return Observable with the new Response state value.
     */
    fun notifyOTAResponseChanged(): Observable<OTAPacket>? {
        return otaResponseCharacteristicWrapper.notifyValueChanged()
    }

    /**
     * Get Observable with the OTA Response value read.
     * @return Observable with the new OTA Response value.
     */
    fun notifyOTAResponseRead(): Observable<OTAPacket>? {
        return otaResponseCharacteristicWrapper.notifyValueRead()
    }
    //endregion
}