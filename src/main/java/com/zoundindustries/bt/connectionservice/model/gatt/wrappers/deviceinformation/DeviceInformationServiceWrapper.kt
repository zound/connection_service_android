package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.deviceinformation

import android.bluetooth.BluetoothGattCharacteristic
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import io.reactivex.Observable

/**
 * Device information GATT service provider (0x180A).
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class DeviceInformationServiceWrapper(private val gattHandler: GattHandler) :
        GattHandler.GattEventListener {

    //region properties
    private val firmwareRevisionProvider = FirmwareRevisionCharacteristicWrapper(gattHandler)
    private val manufacturerNameProvider = ManufacturerNameCharacteristicWrapper(gattHandler)
    private val modelNameProvider = ModelNameCharacteristicWrapper(gattHandler)
    private val serialNumberProvider = SerialNumberCharacteristicWrapper(gattHandler)
    //endregion

    //region public functions
    override fun onCharacteristicRead(characteristic: BluetoothGattCharacteristic) {
        when (characteristic.uuid) {
            CHARACTERISTIC_FIRMWARE_REVISION ->
                firmwareRevisionProvider.onCharacteristicRead(characteristic)
            CHARACTERISTIC_MANUFACTURER_NAME ->
                manufacturerNameProvider.onCharacteristicRead(characteristic)
            CHARACTERISTIC_MODEL_NAME ->
                modelNameProvider.onCharacteristicRead(characteristic)
            CHARACTERISTIC_SERIAL_NUMBER ->
                serialNumberProvider.onCharacteristicRead(characteristic)
        }

    }

    override fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {
        when (characteristic.uuid) {
            CHARACTERISTIC_FIRMWARE_REVISION ->
                firmwareRevisionProvider.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_MANUFACTURER_NAME ->
                manufacturerNameProvider.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_MODEL_NAME ->
                modelNameProvider.onCharacteristicChanged(characteristic)
            CHARACTERISTIC_SERIAL_NUMBER ->
                serialNumberProvider.onCharacteristicChanged(characteristic)
        }
    }

    /**
     * Request to read firmware revision value.
     * The value will be notified asynchronously via notifyFirmwareRevisionRead function as an
     * Observable value.
     */
    fun readFirmwareRevision() {
        firmwareRevisionProvider.readValue()
    }

    /**
     * Get Observable with the firmware revision value changes.
     * @return Observable with the new firmware revision value.
     */
    fun notifyFirmwareRevisionRead(): Observable<String>? {
        return firmwareRevisionProvider.notifyValueRead()
    }

    /**
     * Request to read manufacturer name value.
     * The value will be notified asynchronously via notifyManufacturerNameRead function as an
     * Observable value.
     */
    fun readManufacturerName() {
        manufacturerNameProvider.readValue()
    }

    /**
     * Get Observable with the manufacturer name value changes.
     * @return Observable with the new manufacturer name value.
     */
    fun notifyManufacturerNameRead(): Observable<String>? {
        return manufacturerNameProvider.notifyValueRead()
    }

    /**
     * Request to read model name value.
     * The value will be notified asynchronously via notifyModelNameRead function as an
     * Observable value.
     */
    fun readModelName() {
        modelNameProvider.readValue()
    }

    /**
     * Get Observable with the model name value changes.
     * @return Observable with the new model name value.
     */
    fun notifyModelNameRead(): Observable<String>? {
        return modelNameProvider.notifyValueRead()
    }

    /**
     * Request to read serial number value.
     * The value will be notified asynchronously via notifySerialNumberRead function as an
     * Observable value.
     */
    fun readSerialNumber() {
        serialNumberProvider.readValue()
    }

    /**
     * Get Observable with the serial number value changes.
     * @return Observable with the new serial number value.
     */
    fun notifySerialNumberRead(): Observable<String>? {
        return serialNumberProvider.notifyValueRead()
    }
    //endregion

}