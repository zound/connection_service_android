package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom

import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils.Companion.AB_ID
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils.Companion.getActionIdFromByte

private const val ACTION_BUTTON_CONF_SIZE = 5

val CHARACTERISTIC_ACTION_BUTTON_CONFIG = GattHandler.gattUUIDFromHeader("aa14")

/**
 * Handles action button configuration read, write and notify on characteristic(0xAA14).
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class ABConfigCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<ABConfig>(
                gattHandler.characteristics[CHARACTERISTIC_ACTION_BUTTON_CONFIG],
                gattHandler) {

    @ExperimentalUnsignedTypes
    override fun processWriteValue(data: ABConfig): ByteArray {
        return byteArrayOf(data.buttonId.toByte(),
                data.actions[0].toByte(),
                data.actions[1].toByte(),
                data.actions[2].toByte(),
                data.actions[3].toByte()
        )
    }

    @ExperimentalUnsignedTypes
    override fun processNotifyValue(data: ByteArray): ABConfig {
        if (data.size != ACTION_BUTTON_CONF_SIZE) {
            //TODO: remove for release
            throw IllegalStateException("Wrong size of response - ${data.size}. Expected $ACTION_BUTTON_CONF_SIZE ")
        }
        return ABConfig(AB_ID, arrayOf(
                getActionIdFromByte(data[1].toUByte()),
                getActionIdFromByte(data[2].toUByte()),
                getActionIdFromByte(data[3].toUByte()),
                getActionIdFromByte(data[4].toUByte()))
        )
    }
}