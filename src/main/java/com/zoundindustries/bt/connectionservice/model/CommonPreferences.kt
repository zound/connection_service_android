package com.zoundindustries.bt.connectionservice.model

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.zoundindustries.bt.connectionservice.model.device.DeviceInfo
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.ABUtils
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.custom.EQConfig
import com.zoundindustries.bt.connectionservice.model.spotify.DeviceSpotifyConfig
import com.zoundindustries.bt.connectionservice.model.spotify.SpotifyData
import com.zoundindustries.bt.connectionservice.model.spotify.SpotifyToken

private const val CURRENT_DEVICE_ID = "current_device_id"
private const val PERMISSION_ALREADY_ASKED = "permission_first_ask"
private const val PREVIOUSLY_USED_DEVICES = "previously_used_devices"
private const val SPOTIFY_DID_USER_AUTHORIZE = "spotify_did_user_authorize"
private const val SPOTIFY_WEB_API_TOKEN = "spotify_web_api_token"
private const val SPOTIFY_WEB_API_TOKEN_EXPIRATION_TIMESTAMP = "spotify_web_api_token_expiration_timestamp"
private const val EQ_CUSTOM_PRESET = "eq_custom_preset"


/**
 * Class wrapping access to default SharedPreferences.
 *
 * @property context used to handle resources access for the client.
 * @constructor creates CommonPreferences instance for the specified client.
 */
class CommonPreferences(private val context: Context) {

    //region public functions
    /**
     * Save currently connected/configured device [jsonRawDeviceInfo] to default SharedPreferences.
     * [jsonRawDeviceInfo] is a json built from DeviceInfo object that needs to be saved as a
     * default device
     */
    fun setCachedDeviceInfo(jsonRawDeviceInfo: String?) {
        getEditor().putString(CURRENT_DEVICE_ID, jsonRawDeviceInfo).apply()
    }

    /**
     * Save info that user was alredy asked for permission
     */
    fun setPermissionAlreadyAsked() {
        getEditor().putBoolean(PERMISSION_ALREADY_ASKED, true).apply()
    }

    /**
     * Set list of previously connected devices. [devicesList] must be in JSON format reflecting
     * DeviceInfo class.
     */
    fun setPreviousDevicesRawData(devicesList: String) {
        getEditor().putString(PREVIOUSLY_USED_DEVICES, devicesList).apply()
    }

    /**
     * Set spotify authorization info. If user authorised the app somewhere along the line,
     * this should be true. If we somehow detect that we're not longer authorised, this should be set
     * to false.
     */
    fun setDidUserAuthorizeSpotify(value: Boolean) {
        getEditor().putBoolean(SPOTIFY_DID_USER_AUTHORIZE, value).apply()
    }

    /**
     * Set the Spotify web API access token object
     */
    fun setSpotifyWebApiToken(token: SpotifyToken) {
        getEditor().putString(SPOTIFY_WEB_API_TOKEN, token.value).apply()
        getEditor().putLong(SPOTIFY_WEB_API_TOKEN_EXPIRATION_TIMESTAMP, token.expirationTimestamp).apply()
    }

    /**
     * Set config for spotify action button.
     * Configuration is saved with a device mac address key.
     */
    fun setSpotifyABConfig(deviceId: String, gestureType: ABUtils.GestureType, spotifyData: SpotifyData) {
        val key = getSpotifyDataKey(deviceId)
        val gson = Gson()
        var map = mutableMapOf<ABUtils.GestureType, SpotifyData>()

        if (getPreferences().contains(key)) {
            map = gson.fromJson(
                    getPreferences().getString(key, null),
                    DeviceSpotifyConfig::class.java).configMap

            // because .replace() uses API 24
            if (map.contains(gestureType)) {
                map.remove(gestureType)
            }
        }

        map[gestureType] = spotifyData
        getEditor().putString(key, gson.toJson(DeviceSpotifyConfig(map))).apply()
    }

    /**
     * Set custom Equalizer preset. [jsonRawEQPreset] should already be in Json format parsed from
     * EQConfig object.
     */
    fun setEQCustomPreset(jsonRawEQPreset: String) {
        getEditor().putString(EQ_CUSTOM_PRESET, jsonRawEQPreset).apply()
    }

    /**
     * Get Action Button Spotify configuration of a given device
     */
    fun getSpotifyABConfig(deviceId: String): MutableMap<ABUtils.GestureType, SpotifyData>? {
        val data = getPreferences().getString(getSpotifyDataKey(deviceId), "")

        return if (!data.isNullOrEmpty()) {
            Gson().fromJson(data, DeviceSpotifyConfig::class.java).configMap
        } else null
    }

    /**
     * Get Action Button Spotify configuration for specific gesture of a given device
     */
    fun getSpotifyABConfig(deviceId: String, gestureType: ABUtils.GestureType): SpotifyData? {
        val map = getSpotifyABConfig(deviceId)

        return map?.get(gestureType)
    }

    /**
     * Get currently connected/configured device id from default SharedPreferences.
     * @return the id if exist or empty string if no device was configured.
     */
    fun getCachedDeviceInfo(): DeviceInfo? {
        return Gson().fromJson(getPreferences().getString(CURRENT_DEVICE_ID, ""),
                DeviceInfo::class.java)
    }

    /**
     * Check if user was asked about permission
     */
    fun wasPermissionAlreadyAsked(): Boolean {
        return getPreferences().getBoolean(PERMISSION_ALREADY_ASKED, false)
    }

    /**
     * Get list of previously connected devices String in JSON format.
     */
    fun getPreviousDevicesRawData(): String? {
        return getPreferences().getString(PREVIOUSLY_USED_DEVICES, "")
    }

    /**
     * Check if user already authorized spotify in the app
     */
    fun didUserAuthorizeSpotify(): Boolean? {
        //TODO deduct if user authorised basing on existing token
        return getPreferences().getBoolean(SPOTIFY_DID_USER_AUTHORIZE, false)
    }

    /**
     * Get spotify web API token object
     */
    fun getSpotifyWebApiToken(): SpotifyToken? {
        val value = getPreferences().getString(SPOTIFY_WEB_API_TOKEN, "")
        val expirationTime = getPreferences().getLong(SPOTIFY_WEB_API_TOKEN_EXPIRATION_TIMESTAMP, 0)
        return if (value != null) {
            SpotifyToken(value, expirationTime)
        } else {
            null
        }
    }

    /**
     * Get Equalizer custom preset for currently used headphones.
     * @return EQConfig object parsed from Json.
     */
    fun getEQCustomPreset(): EQConfig {
        return Gson().fromJson(getPreferences().getString(
                EQ_CUSTOM_PRESET,
                Gson().toJson(EQConfig(0,0,0,0,0))),
                EQConfig::class.java)
    }

    //endregion

    //region private functions
    private fun getEditor(): SharedPreferences.Editor {
        return PreferenceManager.getDefaultSharedPreferences(context).edit()
    }

    private fun getPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    private fun getSpotifyDataKey(deviceId: String) = "spotifyData$deviceId"
    //endregion

}