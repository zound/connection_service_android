package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.deviceinformation

import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler

val CHARACTERISTIC_FIRMWARE_REVISION = GattHandler.gattUUIDFromHeader("2a26")

/**
 * Handles firmware revision characteristic(0x2A26)
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class FirmwareRevisionCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<String>(gattHandler.characteristics[CHARACTERISTIC_FIRMWARE_REVISION],
                gattHandler) {
    override fun processWriteValue(data: String): ByteArray {
        return data.toByteArray()
    }

    override fun processNotifyValue(data: ByteArray): String {
        return String(data)
    }
}