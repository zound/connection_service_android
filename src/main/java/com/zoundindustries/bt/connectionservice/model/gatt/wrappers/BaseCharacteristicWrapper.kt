package com.zoundindustries.bt.connectionservice.model.gatt.wrappers

import android.bluetooth.BluetoothGattCharacteristic
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Base class wrapping GATT characteristic handling: reading, writing, notifying.
 * Client class should only provide functions defining data processing (wrapping and unwrapping)
 * by implementing processWriteValue and processNotifyValue.
 *
 * @property characteristic client class must provide this object together with the data value to
 * handle specific characteristic
 * @property gattHandler used to perform GATT operations on the specified characteristic
 */
abstract class BaseCharacteristicWrapper<T>(val characteristic: BluetoothGattCharacteristic?,
                                            private val gattHandler: GattHandler) :
        GattHandler.GattEventListener,
        GattHandler.GattCharacteristicInput<T> {

    private val readCharacteristic: PublishSubject<T> = PublishSubject.create()
    private val changedCharacteristic: PublishSubject<T> = PublishSubject.create()

    //region public functions
    /**
     * Abstract method providing input data processing for the characteristic to be wrttten.
     * Client must specify <T> type of the characteristic [data] and transform it to the
     * ByteArray object.
     *@return transformed data in form of ByteArray ready to be written to GATT characteristic
     */
    abstract fun processWriteValue(data: T): ByteArray

    /**
     * Abstract method providing input data processing for the characteristic to be wrttten.
     * Client must specify <T> type of the characteristic [data] and transform it to the
     * ByteArray object.
     *@return transformed data in form of ByteArray ready to be written to GATT characteristic
     */
    abstract fun processNotifyValue(data: ByteArray): T

    override fun onCharacteristicRead(characteristic: BluetoothGattCharacteristic) {
        readCharacteristic.onNext(processNotifyValue(characteristic.value))
    }

    override fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {
        changedCharacteristic.onNext(processNotifyValue(characteristic.value))
    }

    override fun readValue() {

        characteristic?.let {
            gattHandler.requestReadCharacteristic(characteristic.uuid)
        }
    }

    override fun writeValue(data: T) {
        characteristic?.let {
            gattHandler.requestWriteCharacteristic(characteristic.uuid, processWriteValue(data))
        }
    }

    override fun registerNotification(register: Boolean) {
        characteristic?.let {
            gattHandler.requestNotifyCharacteristic(it, register)
        }
    }

    override fun notifyValueRead(): Observable<T>? {
        return readCharacteristic
    }

    override fun notifyValueChanged(): Observable<T>? {
        return changedCharacteristic
    }
    //end region
}