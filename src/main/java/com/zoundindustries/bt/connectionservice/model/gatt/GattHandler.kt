package com.zoundindustries.bt.connectionservice.model.gatt

import android.annotation.TargetApi
import android.bluetooth.*
import android.content.Context
import android.os.Build
import android.os.Handler
import android.util.ArrayMap
import com.zoundindustries.bt.connectionservice.model.*
import com.zoundindustries.bt.connectionservice.model.device.AdidasBluetoothDevice
import com.zoundindustries.bt.connectionservice.utils.logd
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.*

const val GATT_UUID = "-0000-1000-8000-00805f9b34fb"
const val OTA_UUID = "-d102-11e1-9b23-00025b00a5a5"

private const val REQUEST_MAX_ATTEMPTS = 2
private const val NOTIFICATION_DELAY_MILLIS: Long = 1000

val CLIENT_CHARACTERISTIC_CONFIG = GattHandler.gattUUIDFromHeader("2902")

/**
 * Class handling GATT connection wit the device including all services and characteristics
 * discovery, read, write and notifications. Wraps request queue implementation.
 *
 * @property context used for GATT connection with the device
 */
class GattHandler(private val context: Context) {

    /**
     * Interface defining events notification for the GATT characteristic.
     */
    interface GattEventListener {

        /**
         * Notify the implementing client about GATT [characteristic] read event.
         */
        fun onCharacteristicRead(characteristic: BluetoothGattCharacteristic)

        /**
         * Notify the implementing client about GATT [characteristic] changed event.
         */
        fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic)
    }

    /**
     * Interface defining inputs for GATT service providers.
     */
    interface GattCharacteristicInput<T> {

        /**
         * Request to register for notification of the battery level value.
         * The value will be notified asynchronously via monitorBatteryLevel function as an
         * Observable value.
         */
        fun readValue() {
            //nop
        }

        /**
         * Request to write value to the characteristic wit [data].
         */
        fun writeValue(data: T) {
            //nop
        }

        /**
         * Request to register or unregister for notification of the battery level value depending
         * on [register] parameter.
         * The value will be notified asynchronously via monitorBatteryLevel function as an
         * Observable value.
         */
        fun registerNotification(register: Boolean) {
            //nop
        }

        /**
         * Notify about characteristic value was read with Observable of specified tab.
         * @return Observable on the specified value when read
         */
        fun notifyValueRead(): Observable<T>? {
            return null
        }

        /**
         * Notify about characteristic value changed with Observable of specified tab.
         * @return Observable on the specified value when changed
         */
        fun notifyValueChanged(): Observable<T>? {
            return null
        }
    }

    companion object {
        fun gattUUIDFromHeader(header: String): UUID {
            return UUID.fromString("0000$header$GATT_UUID")
        }

        fun otaUUIDFromHeader(header: String): UUID {
            return UUID.fromString("0000$header$OTA_UUID")
        }
    }

    //region properties
    val characteristics = ArrayMap<UUID, BluetoothGattCharacteristic>()

    private lateinit var bluetoothGatt: BluetoothGatt

    private val bleQueueHandler = BLEQueueHandler()
    private var currentRequest: Request? = null
    private val deviceConnectionState: BehaviorSubject<AdidasBluetoothDevice.ConnectionState> =
            BehaviorSubject.create()
    private val handler = Handler()
    private val readCharacteristic: PublishSubject<BluetoothGattCharacteristic> =
            PublishSubject.create()
    private val changedCharacteristic: PublishSubject<BluetoothGattCharacteristic> =
            PublishSubject.create()
    private val gattCallback = object : BluetoothGattCallback() {

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            getDiscoveredCharacteristics(gatt, status)
        }

        override fun onDescriptorRead(gatt: BluetoothGatt?,
                                      descriptor: BluetoothGattDescriptor?,
                                      status: Int) {
            super.onDescriptorRead(gatt, descriptor, status)
            descriptor.let {
                currentRequest?.let { request ->
                    validateGattResponse(status, request, DESCRIPTOR_READ)
                }
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?,
                                       status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            descriptor?.let {
                currentRequest?.let { request ->
                    validateGattResponse(status, request, DESCRIPTOR_WRITE)
                }
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?,
                                          characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)
            characteristic?.let { gattCharacteristic ->
                currentRequest?.let { request ->
                    validateGattResponse(status, request, CHARACTERISTIC_READ)
                    readCharacteristic.onNext(gattCharacteristic)
                }
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?,
                                             characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)
            characteristic?.let { gattCharacteristic ->
                changedCharacteristic.onNext(gattCharacteristic)
            }
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)
            characteristic?.let {
                currentRequest?.let { request ->
                    validateGattResponse(status, request, CHARACTERISTIC_WRITE)
                }
            }
        }

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            logd("onConnectionStateChanged: $newState status: $status")
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    bluetoothGatt.discoverServices()
                    bluetoothGatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH)
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    deviceConnectionState.onNext(AdidasBluetoothDevice.ConnectionState.DISCONNECTED)
                }
            }
        }
    }

    private fun validateGattResponse(status: Int, request: Request, expectedType: Int) {
        if (status != BluetoothGatt.GATT_SUCCESS) {
            onRequestFailed(request)
        } else if (request.type == expectedType) {
            bleQueueHandler.processNextRequest()
        }
    }
//endregion

//region public functions
    /**
     * Establishes GATT connection to [device] BluetoothDevice to start getting data over BLE.
     * Method will try to connect to the device whenever it is available - autoConnect option is
     * set to true by default.
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun connect(device: AdidasBluetoothDevice) {
        deviceConnectionState.onNext(AdidasBluetoothDevice.ConnectionState.CONNECTING)
        device.bluetoothDevice.let { btDevice ->
            bluetoothGatt = btDevice.connectGatt(context,
                    true,
                    gattCallback,
                    BluetoothDevice.TRANSPORT_LE)
        }

    }

    /**
     * Request to register or unregister for the passed [characteristic]. Register by setting
     * [shouldRegister] to true and unregister by setting it to false.
     * @return false if one of the following reasons occurred:
     *    The device is not connected./li>
     *    The given characteristic UUID does not correspond to any of the available characteristics of the connected device.
     *    The characteristic does not have the "notify" property.
     */
    fun requestNotifyCharacteristic(characteristic: BluetoothGattCharacteristic,
                                    shouldRegister: Boolean): Boolean {
        if (deviceConnectionState.value == AdidasBluetoothDevice.ConnectionState.CONNECTED) {
            addNotificationRequest(characteristic, shouldRegister)
        }
        return true
    }

    /**
     * Request to read the value for the characteristic with the passed [uuid].
     * @return false if one of the following reasons occurred:
     *    The device is not connected./li>
     *    The given characteristic UUID does not correspond to any of the available characteristics of the connected device.
     *    The characteristic does not have the "notify" property.
     */
    fun requestReadCharacteristic(uuid: UUID): Boolean {
        if (deviceConnectionState.value == AdidasBluetoothDevice.ConnectionState.CONNECTED) {
            val characteristic = characteristics[uuid]
            characteristic?.let {
                if (characteristic.properties and BluetoothGattCharacteristic.PROPERTY_READ <= 0) {
                    return false
                }
                val request = Request.createCharacteristicReadRequest(characteristic)
                bleQueueHandler.addToRequestsQueue(request)
            }
        } else {
            return false
        }
        return true
    }

    /**
     * Request to write the [data] for the characteristic with the passed [uuid].
     * @return false if one of the following reasons occurred:
     *    The device is not connected./li>
     *    The given characteristic UUID does not correspond to any of the available characteristics of the connected device.
     *    The characteristic does not have the "notify" property.
     */
    fun requestWriteCharacteristic(uuid: UUID, data: ByteArray): Boolean {
        if (deviceConnectionState.value == AdidasBluetoothDevice.ConnectionState.CONNECTED) {
            val characteristic = characteristics[uuid]
            characteristic?.let {
                if (characteristic.properties and BluetoothGattCharacteristic.PROPERTY_WRITE <= 0) {
                    return false
                }
                val request = Request.createCharacteristicWriteRequest(characteristic, data)
                bleQueueHandler.addToRequestsQueue(request)
            }
        } else {
            return false
        }
        return true
    }

    /**
     * Request to write the [data] for the characteristic with the passed [uuid] with no response.
     * @return false if one of the following reasons occurred:
     *    The device is not connected.
     *    The given characteristic UUID does not correspond to any of the available characteristics of the connected device.
     *    The characteristic does not have the "notify" property.
     */
    fun requestWriteCharacteristicNoResponse(uuid: UUID, data: ByteArray): Boolean {
        if (deviceConnectionState.value == AdidasBluetoothDevice.ConnectionState.CONNECTED) {
            val characteristic = characteristics[uuid]
            characteristic?.let {
                if (characteristic.properties and BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE <= 0) {
                    return false
                }
                val request = Request.createCharacteristicWriteNoResponseRequest(characteristic, data)
                bleQueueHandler.addToRequestsQueue(request)
            }
        } else {
            return false
        }
        return true
    }

    /**
     * Read the [characteristic] using gatt connection.
     * @return false if one of the following reasons occurred:
     *    GATT is not initialized
     *    Framework read call failed
     */
    fun readCharacteristic(characteristic: BluetoothGattCharacteristic): Boolean {
        if (!::bluetoothGatt.isInitialized) {
            return false
        }
        return bluetoothGatt.readCharacteristic(characteristic)
    }

    /**
     * Write the [characteristic] using gatt connection with response [withResponse] flag.
     * @return false if one of the following reasons occurred:
     *    GATT is not initialized
     *    Framework write call failed
     */
    fun writeCharacteristic(characteristic: BluetoothGattCharacteristic,
                            withResponse: Boolean): Boolean {
        if (!::bluetoothGatt.isInitialized) {
            return false
        }
        characteristic.writeType = if (withResponse) {
            BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        } else {
            BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE
        }
        return bluetoothGatt.writeCharacteristic(characteristic)
    }


    /**
     * Register for the [characteristic] notifications.
     * @return false if one of the following reasons occurred:
     *    GATT is not initialized
     *    Framework notification registration call failed
     */
    fun registerForNotification(characteristic: BluetoothGattCharacteristic): Boolean {
        if (!::bluetoothGatt.isInitialized) {
            return false
        }
        return bluetoothGatt.setCharacteristicNotification(characteristic, true)
    }

    /**
     * Write the [descriptor] using gatt connection.
     * @return false if one of the following reasons occurred:
     *    GATT is not initialized
     *    Framework write call failed
     */
    fun writeDescriptor(descriptor: BluetoothGattDescriptor): Boolean {
        if (!::bluetoothGatt.isInitialized) {
            return false
        }
        return bluetoothGatt.writeDescriptor(descriptor)
    }

    /**
     * Write the [descriptor] using gatt connection.
     * @return false if one of the following reasons occurred:
     *    GATT is not initialized
     *    Framework read call failed
     */
    fun readDescriptor(descriptor: BluetoothGattDescriptor): Boolean {
        if (!::bluetoothGatt.isInitialized) {
            return false
        }
        return bluetoothGatt.readDescriptor(descriptor)
    }

    /**
     * Notify with Observable when AdidasBluetoothDevice connection state is changed.
     * Notification will emit ConnectionState after connecting to a device with connect() function.
     * @return Observable for AdidasBluetoothDevice connection state changes.
     */
    fun notifyDeviceConnectionState(): Observable<AdidasBluetoothDevice.ConnectionState> {
        return deviceConnectionState
    }

    /**
     * Notify with Observable when AdidasBluetoothDevice connection state is changed.
     * Notification will emit ConnectionState after connecting to a device with connect() function.
     * @return Observable for AdidasBluetoothDevice connection state changes.
     */
    fun notifyReadCharacteristic(): Observable<BluetoothGattCharacteristic> {
        return readCharacteristic
    }

    /**
     * Notify with Observable when AdidasBluetoothDevice connection state is changed.
     * Notification will emit ConnectionState after connecting to a device with connect() function.
     * @return Observable for AdidasBluetoothDevice connection state changes.
     */
    fun notifyChangedCharacteristic(): Observable<BluetoothGattCharacteristic> {
        return changedCharacteristic
    }

    /**
     * Cancel ongoing GATT connection for currently connected device.
     */
    fun disconnectCurrentDevice() {
        if (this::bluetoothGatt.isInitialized) {
            bluetoothGatt.disconnect()
            bluetoothGatt.close()
            bleQueueHandler.resetQueue()
            deviceConnectionState.onNext(AdidasBluetoothDevice.ConnectionState.DISCONNECTED)
        }
    }

    /**
     * Reset current queue state, remove all pending requests.
     */
    fun resetQueue() {
        bleQueueHandler.resetQueue()
    }
    //endregion

    //region private functions
    private fun getDiscoveredCharacteristics(gatt: BluetoothGatt?, status: Int) {
        if (status == BluetoothGatt.GATT_SUCCESS) {
            gatt?.let { bluetoothGatt ->
                bluetoothGatt.services.forEach { gatService ->
                    gatService.characteristics.forEach { characteristic ->
                        characteristics[characteristic.uuid] = characteristic
                    }
                }
                deviceConnectionState.onNext(AdidasBluetoothDevice.ConnectionState.CONNECTED)
            }
        } else {
            disconnectCurrentDevice()
            deviceConnectionState.onNext(AdidasBluetoothDevice.ConnectionState.DISCONNECTED)
        }
    }

    private fun addNotificationRequest(characteristic: BluetoothGattCharacteristic,
                                       shouldRegister: Boolean) {
        bleQueueHandler.addToRequestsQueue(
                Request.createCharacteristicNotificationRequest(characteristic))
        bleQueueHandler.addToRequestsQueue(
                setupNotificationResponse(shouldRegister, characteristic))
    }

    private fun setupNotificationResponse(shouldRegister: Boolean,
                                          characteristic: BluetoothGattCharacteristic): Request {
        val data = if (shouldRegister) {
            BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        } else {
            BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
        }
        return Request.createDescriptorWriteRequest(
                characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG), data)
    }

    private fun onRequestFailed(request: Request) {
        if (request.attempts < REQUEST_MAX_ATTEMPTS) {
            bleQueueHandler.addToRequestsQueue(request)
        }
        bleQueueHandler.processNextRequest()
    }
    //endregion

    /**
     * Class wrapping request queue implementation
     */
    private inner class BLEQueueHandler {

        //region properties
        private var isQueueProcessing = false
        private val requestQueue: Queue<Request> = LinkedList()
        //endregion

        //region public functions
        /**
         * Add [request] to the queue for further processing.
         * Next request will be processed when the current one is acknowledged or failed.
         */
        fun addToRequestsQueue(request: Request) {
            if (request.attempts < REQUEST_MAX_ATTEMPTS) {
                requestQueue.add(request)
            }

            if (!isQueueProcessing) {
                processNextRequest()
            }
        }

        fun processNextRequest() {
            if (!validateRequest()) return

            val request = requestQueue.remove()
            currentRequest = request
            request.attempts = request.attempts.inc()

            when (request.type) {
                CHARACTERISTIC_READ -> {
                    val readCharacteristic = request.characteristic
                    readCharacteristic?.let { characteristic ->
                        if (!readCharacteristic(characteristic))
                            onRequestFailed(request)
                    }
                }
                CHARACTERISTIC_WRITE -> {
                    val writeCharacteristic = request.characteristic
                    writeCharacteristic?.let { characteristic ->
                        if (!writeCharacteristic(characteristic, true))
                            onRequestFailed(request)
                    }
                }
                CHARACTERISTIC_WRITE_NO_RESPONSE -> {
                    val writeCharacteristic = request.characteristic
                    writeCharacteristic?.let { characteristic ->
                        if (!writeCharacteristic(characteristic, false))
                            onRequestFailed(request)
                    }
                }
                CHARACTERISTIC_NOTIFICATION -> {
                    val notifyCharacteristic = request.characteristic
                    notifyCharacteristic?.let { characteristic ->
                        if (!registerForNotification(characteristic))
                            onRequestFailed(request)
                    }
                    scheduleNotificationProcessing()
                }
                DESCRIPTOR_WRITE -> {
                    val writeDescriptor = request.descriptor
                    writeDescriptor?.let { descriptor ->
                        if (!writeDescriptor(descriptor))
                            onRequestFailed(request)
                    }
                }
                DESCRIPTOR_READ -> {
                    val readDescriptor = request.descriptor
                    readDescriptor?.let { descriptor ->
                        if (!readDescriptor(descriptor))
                            onRequestFailed(request)
                    }
                }
            }
        }

        /**
         * Reset current queue state, remove all pending requests.
         */
        fun resetQueue() {
            requestQueue.clear()
            isQueueProcessing = false
            //TODO add request timeout
        }
        //endregion

        //region private functions
        private fun scheduleNotificationProcessing() {
            run {
                handler.postDelayed({ processNextRequest() }, NOTIFICATION_DELAY_MILLIS)
            }
        }

        private fun validateRequest(): Boolean {
            isQueueProcessing = true

            //TODO add request timeout
            if (requestQueue.size <= 0) {
                isQueueProcessing = false
                return false
            }

            if (deviceConnectionState.value != AdidasBluetoothDevice.ConnectionState.CONNECTED) {
                resetQueue()
                return false
            }
            return true
        }
        //endregion
    }
}