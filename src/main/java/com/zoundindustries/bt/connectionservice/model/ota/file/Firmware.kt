package com.zoundindustries.bt.connectionservice.model.ota.file

/**
 * Class wrapping firmware meta data.
 *
 * @property name of the FW binary file.
 * @property size of the FW binary file
 * @property md5 checksum of the FW binary file.
 */
data class Firmware(val name: String, val size: String, val md5: String)