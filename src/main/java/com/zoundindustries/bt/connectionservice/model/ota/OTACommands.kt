package com.zoundindustries.bt.connectionservice.model.ota

/**
 * Class wrapping constant values for VM Upgrade commands.
 */
class OTACommands {
    companion object {
        //region VM commands
        /**
         * Begins a VM Upgrade session , allowing VM Upgrade Protocol packets to be sent using the
         * VM Upgrade Control and VM Upgrade Data commands.
         */
        const val COMMAND_VM_UPGRADE_CONNECT = 0x0640

        /**
         * Tunnels a VM Upgrade Protocol packet.
         */
        const val COMMAND_VM_UPGRADE_CONTROL = 0x0642

        /**
         * Closes VM session.
         */
        const val COMMAND_VM_UPGRADE_DISCONNECT = 0x0641

        //endregion

        //region notification commands
        /**
         *
         * Hosts register for notifications using the `REGISTER_NOTIFICATION` command, specifying an
         * Event type from the below possible packets as the first byte of payload,
         * with optional parameters as defined per event in successive payload bytes.
         */
        const val COMMAND_REGISTER_NOTIFICATION = 0x4001

        /**
         * Packet used to register VMU notifications.
         */
        const val VMU_PACKET = 0x12
        //endregion

        //region responses

        /**
         * Generic VM Upgrade acknowledge message confirming that VM Upgrade command was sent.
         */
        const val ACK_VM_UPGRADE = 0x8640

        /**
         * Generic VM Upgrade Control acknowledge message confirming that VM Upgrade Control
         * command was sent.
         */
        const val ACK_VM_UPGRADE_CONTROL = 0x8642

        /**
         * Packet header defining the VM Upgrade notification. Further data in the notification
         * packet depends on the UpgradeOperation type of the notification.
         */
        const val UPGRADE_COMMAND_NOTIFICATION = 0x4003

        /**
         * Acknowledgment of notification being registered.
         */
        const val ACK_REGISTER_NOTIFICATION = 0xc001
        //endregion
    }
}