package com.zoundindustries.bt.connectionservice.model.gatt.wrappers.ota

import android.util.Log
import com.zoundindustries.bt.connectionservice.model.gatt.GattHandler
import com.zoundindustries.bt.connectionservice.model.gatt.wrappers.BaseCharacteristicWrapper
import com.zoundindustries.bt.connectionservice.model.ota.packet.OTAPacket

val CHARACTERISTIC_OTA_RESPONSE = GattHandler.otaUUIDFromHeader("1102")

/**
 * Handles OTA Response read, write and notify on characteristic(0x1101).
 *
 * @property gattHandler providing GATT functionality to be used by service provider.
 */
class OTAResponseCharacteristicWrapper(private val gattHandler: GattHandler) :
        BaseCharacteristicWrapper<OTAPacket>(
                gattHandler.characteristics[CHARACTERISTIC_OTA_RESPONSE],
                gattHandler) {
    override fun processWriteValue(data: OTAPacket): ByteArray {
        return data.payload
    }

    override fun processNotifyValue(data: ByteArray): OTAPacket {
        val otaPacket = OTAPacket(data)
        logResponse(data)
        return otaPacket
    }

    private fun logResponse(data: ByteArray) {
        var bytes = ""
        data.forEach {
            bytes += " ${String.format("0x%02X", it)}"
        }
        Log.d("OTAResponseProvider", " RESPONSE:  $bytes")
    }
}