package com.zoundindustries.bt.connectionservice.model

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * Class wrapping receivers for system bluetooth availability and state.
 * This class handles BT stae registration and unregistration and intent handling.
 *
 * @property context used to register btStateReceiver.
 */
class SystemBluetoothStateReceiver(private val context: Context) {

    private val btState: PublishSubject<Boolean> = PublishSubject.create()
    private val btBondState: PublishSubject<BondEvent> = PublishSubject.create()

    private lateinit var btStateReceiver: BroadcastReceiver
    private lateinit var btBondStateReceiver: BroadcastReceiver

    init {
        setupBTStateReceiver()
        setupBTBondStateReceiver()
    }

    /**
     * Notify when bluetooth is enabled or disabled on the device.
     * @return observable with state flag.
     */
    fun notifyBluetoothState(): Observable<Boolean> {
        return btState
    }

    /**
     * Notify when bond state of the device has changed.
     * @return observable with device address and bond state.
     */
    fun notifyBluetoothBondState(): Observable<BondEvent> {
        return btBondState
    }

    /**
     * Clear all BT related receivers
     */
    fun dispose() {
        context.unregisterReceiver(btStateReceiver)
        context.unregisterReceiver(btBondStateReceiver)
    }

    private fun setupBTStateReceiver() {
        btStateReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent ?: return
                if (BluetoothAdapter.ACTION_STATE_CHANGED == intent.action) {
                    val extraState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)
                    when (extraState) {
                        BluetoothAdapter.STATE_OFF -> btState.onNext(false)
                        BluetoothAdapter.STATE_ON -> btState.onNext(true)
                    }
                }
            }
        }
        context.registerReceiver(btStateReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
    }

    private fun setupBTBondStateReceiver() {
        btBondStateReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent ?: return
                if (ACTION_BOND_STATE_CHANGED == intent.action) {
                    val device = intent.getParcelableExtra<BluetoothDevice>(EXTRA_DEVICE)
                    val bondState = intent.getIntExtra(EXTRA_BOND_STATE, -1)
                    btBondState.onNext(BondEvent(device.address, bondState))
                }
            }
        }
        context.registerReceiver(btBondStateReceiver, IntentFilter(ACTION_BOND_STATE_CHANGED))
    }
}