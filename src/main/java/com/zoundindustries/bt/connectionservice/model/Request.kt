package com.zoundindustries.bt.connectionservice.model

import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor

const val CHARACTERISTIC_NOTIFICATION = 0
const val CHARACTERISTIC_READ = 1
const val CHARACTERISTIC_WRITE = 2
const val CHARACTERISTIC_WRITE_NO_RESPONSE = 3
const val DESCRIPTOR_READ = 4
const val DESCRIPTOR_WRITE = 5

/**
 * Class wrapping request for different value of characteristic action described in [type].
 * //TODO: Probably we should use inheritance and factory pattern here for Request types.
 */
class Request(type: Int,
              characteristic: BluetoothGattCharacteristic? = null,
              descriptor: BluetoothGattDescriptor? = null,
              data: ByteArray? = null) {


    //region properties
    var attempts = 0

    var characteristic = characteristic
        private set
    var descriptor = descriptor
        private set
    var data = data
        private set
    var type = type
        private set
    //endregion

    companion object {

        /**
         * Create Request of value CHARACTERISTIC_NOTIFICATION with the passed [characteristic]
         * value.
         * @return newly created Request
         */
        fun createCharacteristicNotificationRequest(characteristic: BluetoothGattCharacteristic):
                Request {
            return Request(CHARACTERISTIC_NOTIFICATION, characteristic)
        }

        /**
         * Create Request of value CHARACTERISTIC_READ with the passed [characteristic]
         * value.
         * @return newly created Request
         */
        fun createCharacteristicReadRequest(characteristic: BluetoothGattCharacteristic): Request {
            return Request(CHARACTERISTIC_READ, characteristic)
        }

        /**
         * Create Request of value CHARACTERISTIC_WRITE with the passed [characteristic] value
         * and [data] to be written.
         * @return newly created Request
         */
        fun createCharacteristicWriteRequest(characteristic: BluetoothGattCharacteristic,
                                             data: ByteArray): Request {
            characteristic.value = data
            return Request(CHARACTERISTIC_WRITE, characteristic)
        }

        /**
         * Create Request of value CHARACTERISTIC_WRITE_NO_RESPONSE with the passed [characteristic]
         * value and [data] to be written.
         * @return newly created Request
         */
        fun createCharacteristicWriteNoResponseRequest(characteristic: BluetoothGattCharacteristic,
                                                       data: ByteArray): Request {
            characteristic.value = data
            return Request(CHARACTERISTIC_WRITE_NO_RESPONSE, characteristic)
        }

        /**
         * Create Request of value DESCRIPTOR_READ with the passed [descriptor] value.
         * @return newly created Request
         */
        fun createDescriptorReadRequest(descriptor: BluetoothGattDescriptor): Request {
            return Request(DESCRIPTOR_READ, null, descriptor)
        }

        /**
         * Create Request of value DESCRIPTOR_WRITE with the passed [descriptor] value and [data]
         * to be written.
         * @return newly created Request
         */
        fun createDescriptorWriteRequest(descriptor: BluetoothGattDescriptor,
                                         data: ByteArray): Request {
            descriptor.value = data
            return Request(DESCRIPTOR_WRITE, null, descriptor)
        }
    }
}