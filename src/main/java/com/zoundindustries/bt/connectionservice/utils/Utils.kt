package com.zoundindustries.bt.connectionservice.utils

import android.content.Context
import android.os.CountDownTimer
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.zoundindustries.bt.connectionservice.model.CommonPreferences
import com.zoundindustries.bt.connectionservice.model.device.DeviceInfo

/**
 * Simplified timer, which takes only one time parameter and does not require overriding onTick
 */
abstract class SimplifiedTimer(period: Long) : CountDownTimer(period, period) {
    override fun onTick(millisUntilFinished: Long) {
        //nop
    }
}

/**
 * Wrap general app utils.
 */
class Utils {

    companion object {
        /**
         * Parse device in JSON format to ArrayList using [context] to access preferences.
         * @return ArrayList<DeviceInfo> with previously used devices.
         */
        fun getDevicesListFromJson(context: Context): ArrayList<DeviceInfo> {
            return Gson().fromJson(CommonPreferences(context)
                    .getPreviousDevicesRawData(), object : TypeToken<ArrayList<DeviceInfo>>() {}.type)
        }

        /**
         * Add [deviceInfo] to previously used devices using [context].
         */
        fun addDeviceToHistory(deviceInfo: DeviceInfo, context: Context) {
            var deviceList: ArrayList<DeviceInfo>? =
                    Gson().fromJson(
                            CommonPreferences(context).getPreviousDevicesRawData(),
                            object : TypeToken<ArrayList<DeviceInfo>>() {}.type)

            if (deviceList == null) {
                deviceList = ArrayList()
            } else if (deviceList.contains(deviceInfo)) {
                return
            }

            deviceList.add(deviceInfo)
            CommonPreferences(context).setPreviousDevicesRawData(Gson().toJson(deviceList))
        }
    }
}
