package com.zoundindustries.bt.connectionservice.utils

import android.content.Context
import android.util.Log
import com.zoundindustries.bt.connectionservice.ConnectionServiceApplication
import com.zoundindustries.bt.connectionservice.BuildConfig

/**
 * Extension method providing encapsulation of class name tag and printing [message] info into
 * standard Android log buffer.
 */
fun Any.logd(message: String) {
    if (BuildConfig.DEBUG) Log.d(this::class.simpleName, message)
}

/**
 * get current time in seconds format
 */
fun currentTimeSeconds(): Long {
    return System.currentTimeMillis() / 1000
}

/**
 * Extension val for ConnectionServiceApplication.
 */
val Context.adidasApplication: ConnectionServiceApplication
    get() = applicationContext as ConnectionServiceApplication