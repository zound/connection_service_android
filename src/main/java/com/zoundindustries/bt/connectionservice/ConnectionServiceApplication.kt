package com.zoundindustries.bt.connectionservice

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.zoundindustries.bt.connectionservice.service.BLEService
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * Application class providing wrapper for connecting to BleService.
 */
class ConnectionServiceApplication : Application() {
    private val serviceConnectionSubject = BehaviorSubject.create<Boolean>()

    /**
     * BleService instance used by all clients
     */
    var bleService: BLEService? = null
        private set

    override fun onCreate() {
        super.onCreate()
        startService(Intent(this, BLEService::class.java))
        connectDeviceService()
    }
    /**
     * Notify when BleService has been connected.
     * @return Boolean observable indicating service state
     */
    fun notifyServiceConnected(): Observable<Boolean> {
        return serviceConnectionSubject
    }

    private fun connectDeviceService() {
        val connection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
                val binder = iBinder as BLEService.LocalBinder
                bleService = binder.getService()
                serviceConnectionSubject.onNext(true)
            }

            override fun onServiceDisconnected(componentName: ComponentName) {
                serviceConnectionSubject.onNext(false)
            }
        }

        val intent = Intent(this, BLEService::class.java)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }
}